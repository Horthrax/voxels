#ifndef OCTREE_H
#define OCTREE_H

typedef struct octree_info
{
    u16 child_index;
    u8 valid_children;
    u8 child_leafs;
} octree_info;

typedef struct octree
{
    u32 NodeSize;
    octree_info *Data; 
    u32 LastOffset;
    ID3D11Buffer *VertexBuffer;
    int VertexCount;
} octree;

octree CreateOctree(u32 NodeSize);
void AddCell(octree *Octree, u32 x, u32 y, u32 z);
void MeshOctree(octree *Octree);
#endif //OCTREE_H
