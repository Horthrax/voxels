// Copyright Rageface Studios, LTD. All Rights Reserved.
#define CINTERFACE
#define COBJMACROS
#define D3D11_NO_HELPERS

#include <d3d11.h>

#include "rf_platform.h"

#include "imgui.h"

#if !DISABLE_HOT_RELOADING
#include "voxels_dll.cpp"
#endif

#include "voxels_math.h"

#define HANDMADE_MATH_IMPLEMENTATION
#define HMM_SINF VoxelsSinF
#define HMM_COSF VoxelsCosF
#define HMM_ACOSF 
#define HMM_TANF 
#include "handmademath_internal.h"


#define STB_PERLIN_IMPLEMENTATION
#include "stb_perlin.h"

#include "voxels_types.h"
#include "voxels.h"
#include "input_handler.h"

#include "voxels_world.cc"

#define HEIGHTMAP 0

static ID3D11Device* Device;
static ID3D11DeviceContext* DeviceContext;
static ID3D11RenderTargetView** RenderTargetView;
static ID3D11DepthStencilView** DepthStencilView;

static world World;

static ID3D11Buffer* ConstantBuffer;

static camera Camera;
static int VertexCount;

static float Sensitivity = 0.05f;
static float CameraSpeed = 0.5f;
static bool DebugMode;

typedef struct chunk_mesher_worker_information
{
    world *World;
    position Position;
    u32 Index;
    ID3D11Device *Device;
} chunk_mesher_worker_information;

typedef struct chunk_generator_worker_information
{
    world *World;
    position Position;
    u32 Index;
} chunk_generator_worker_information;

#if HEIGHTMAP
int HeightmapVertexCount = 0;
#endif

static read_entire_file
ReadEntireFile(char *FilePath)
{
    read_entire_file Result = {0};
    
    rf_file File = RfPlatform.OpenFile(FilePath);
    if(File.IsValid)
    {
        size_t FileSize = RfPlatform.GetFileSize(&File);
        if(FileSize)
        {
            Result.Contents = RfPlatform.AllocateMemory(FileSize);
            if(Result.Contents)
            {                
                if(RfPlatform.ReadFile(&File, Result.Contents, FileSize))
                {
                    Result.ContentsSize = FileSize;
                }                
            }
        }
    }
    return(Result);
}

#if !HEIGHTMAP
static RF_PLATFORM_WORK_QUEUE_CALLBACK(ChunkMesherWorker)
{
    chunk_mesher_worker_information MeshInput = *((chunk_mesher_worker_information *)Data);
    MeshChunk(MeshInput.World, MeshInput.Position, MeshInput.Index, MeshInput.Device);
    free(Data);
}

static RF_PLATFORM_WORK_QUEUE_CALLBACK(ChunkGeneratorWorker)
{
    chunk_generator_worker_information GeneratorInput = *((chunk_generator_worker_information *)Data);
    GenerateChunk(GeneratorInput.World, GeneratorInput.Position, GeneratorInput.Index);
    free(Data);
}
#else

vertex_buffer_wrapper
GenerateHeightmapVBO()
{
    int Dimension = 2048; //4096
    u64 CubeCount = Dimension*Dimension;
    u64 VertexCount = CubeCount*36;
    int Channels = 3;
    unsigned char *HeightmapData = stbi_load("../data/voxel_heightmap.png", &Dimension, &Dimension, &Channels, 0);
    unsigned char *HeightmapColorData = stbi_load("../data/voxel_heightmap_colors.png", &Dimension, &Dimension, &Channels, 0);
    u64 SizeTest = sizeof(cube)*CubeCount;
    cube *Cubes = (cube *)VirtualAlloc(0, sizeof(vertex)*VertexCount, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
    //memset(Cubes, 0, SizeTest);
    u64 DimTest = 2048;
    hmm_vec4 blue = HMM_Vec4(0.0f, 1.0f, 0.0f, 1.0f);
    for(int i = 0; i < DimTest*DimTest; i++)
    {
        unsigned short Height = *(HeightmapData + i*3);
        unsigned short X = i%Dimension;
        unsigned short Z = (i/Dimension)%Dimension;
        
        unsigned char r = *(HeightmapColorData + i*3);
        unsigned char g = *(HeightmapColorData + (i*3)+1);
        unsigned char b = *(HeightmapColorData + (i*3)+2);
        
        float RGBEffect = 1.0/255.0;
        position BlockPosition = {X, (unsigned short)Height, Z};
        // TODO(Horthrax): messed up because it requires more than u8 X,Y,Z values
        AddCube(Cubes+i, X, Height/4, Z, r, g, b, 20, 1);
    }
    
    ID3D11Buffer *VertexBuffer;    
    D3D11_BUFFER_DESC BufferDescription = {0};
    BufferDescription.Usage = D3D11_USAGE_DEFAULT;
    BufferDescription.ByteWidth = sizeof(vertex)*VertexCount;
    BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    BufferDescription.CPUAccessFlags = 0;
    
    D3D11_SUBRESOURCE_DATA SubresourceData = {0};
    SubresourceData.pSysMem = Cubes;
    
    if(FAILED(Device->lpVtbl->CreateBuffer(Device, &BufferDescription, &SubresourceData, &VertexBuffer)))
    {
        // TODO(zak): Logging
    }
    
    UINT Stride = sizeof(vertex);
    UINT Offset = 0;
    DeviceContext->lpVtbl->IASetVertexBuffers(DeviceContext, 0, 1, &VertexBuffer, &Stride, &Offset);
    RfPlatform.FreeMemory(Cubes);
    return (vertex_buffer_wrapper){VertexCount, VertexBuffer};
}
#endif

static void 
PlatformLayer_Init(rf_platform *InPlatform)
{
    // NOTE(zak): Passing in the platform we set in the exe, so that the DLL 
    // can call back to the EXE to get info. This really shouldnt happen that often    
    RfPlatform = *InPlatform;
    
    RfPlatform.SetMouseMode(RF_MOUSE_MODE_GAME);
    
    Device = (ID3D11Device *)RfPlatform.D3DGetDevice();
    DeviceContext = (ID3D11DeviceContext *)RfPlatform.D3DGetDeviceContext();
     RenderTargetView = (ID3D11RenderTargetView **)RfPlatform.D3DGetRenderTargetView();
    DepthStencilView = (ID3D11DepthStencilView**)RfPlatform.D3DGetDepthStencilView();
    
    ID3D11VertexShader *VertexShader;             
    read_entire_file VertexFile = ReadEntireFile("../code/shaders/basic_vertex.hlslc");
    if(VertexFile.ContentsSize)
    {
        if(Device->lpVtbl->CreateVertexShader(Device, VertexFile.Contents, VertexFile.ContentsSize, 0, &VertexShader) != S_OK)
        {
            // TODO(zak): Logging
        }     
    }

    ID3D11PixelShader* PixelShader;
    read_entire_file PixelFile = ReadEntireFile("../code/shaders/basic_pixel.hlslc");
    if(PixelFile.ContentsSize)
    {
        if(Device->lpVtbl->CreatePixelShader(Device, PixelFile.Contents, PixelFile.ContentsSize, 0, &PixelShader) != S_OK)
        {
            // TODO(zak): Logging
        }
    }     
    
    
    ID3D11InputLayout* VertexLayout;
    D3D11_INPUT_ELEMENT_DESC Layout[2];
    Layout[0] = (D3D11_INPUT_ELEMENT_DESC){"POSITION", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, 0, D3D11_INPUT_PER_VERTEX_DATA, 0};    
    Layout[1] = (D3D11_INPUT_ELEMENT_DESC){"COLOR", 0, DXGI_FORMAT_R8G8B8A8_UINT, 0, D3D11_APPEND_ALIGNED_ELEMENT, D3D11_INPUT_PER_VERTEX_DATA, 0};
    
    if(Device->lpVtbl->CreateInputLayout(Device, Layout, 2, VertexFile.Contents, VertexFile.ContentsSize, &VertexLayout) == S_OK)
    {
        DeviceContext->lpVtbl->IASetInputLayout(DeviceContext, VertexLayout);        
    }
    else
    {
        // TODO(zak): Logging
    }
    
    RfPlatform.FreeMemory(PixelFile.Contents);
    RfPlatform.FreeMemory(VertexFile.Contents);
    
    float size = 1.0f;
    
    // Create WVP buffer    
    D3D11_BUFFER_DESC ConstantBufferDescription = {0};
    ConstantBufferDescription.Usage = D3D11_USAGE_DEFAULT;
    ConstantBufferDescription.ByteWidth = sizeof(constant_buffer);
    ConstantBufferDescription.BindFlags = D3D11_BIND_CONSTANT_BUFFER;
    ConstantBufferDescription.CPUAccessFlags = 0;
    ConstantBufferDescription.MiscFlags = 0;
    
    if(FAILED(Device->lpVtbl->CreateBuffer(Device, &ConstantBufferDescription, 0, &ConstantBuffer)))
    {
        // TODO(zak): Logging
    }
    
    DeviceContext->lpVtbl->IASetPrimitiveTopology(DeviceContext, D3D11_PRIMITIVE_TOPOLOGY_TRIANGLELIST);
    
    DeviceContext->lpVtbl->VSSetShader(DeviceContext, VertexShader, 0, 0);
    DeviceContext->lpVtbl->PSSetShader(DeviceContext, PixelShader, 0, 0);
    
    Camera.Yaw = -90.0f;
    
    Camera.Position = HMM_Vec3(0.0f, 0.0f, -1.0f);
    Camera.Front = HMM_Vec3(0.0f, -1.0f, 0.0f);
    Camera.Up = HMM_Vec3(0.0f, 1.0f, 0.0f);
    
#if HEIGHTMAP
    UINT Stride = sizeof(vertex);
    UINT Offset = 0;
    vertex_buffer_wrapper VBW = GenerateHeightmapVBO();
    HeightmapVertexCount = VBW.VertexCount;
    DeviceContext->lpVtbl->IASetVertexBuffers(DeviceContext, 0, 1, &VBW.VertexBuffer, &Stride, &Offset);
#else
    
#endif
    u8 Y = 4;
    u64 mask = (~(~0U << 5)) << Y;
    u64 TestValue = (~(~0U << 5)) << Y;
    u64 TestValue2 = (~(~0U << 5)) << (Y+1);
    bool AndEvaluation = (TestValue & mask) == (TestValue2 & mask);
    u8 test;
}

static void 
PlatformLayer_Shutdown()
{
    
}

void
UpdateCamera(float MouseXDifferenceAdjusted, float MouseYDifferenceAdjusted)
{
    Camera.Yaw -= MouseXDifferenceAdjusted;
    Camera.Pitch -= MouseYDifferenceAdjusted;
    
    if(Camera.Pitch > 89.0f) Camera.Pitch = 89.0f;
    if(Camera.Pitch < -89.0f) Camera.Pitch = -89.0f;
    
    Camera.Front = HMM_NormalizeVec3(HMM_Vec3(HMM_CosF(HMM_ToRadians(Camera.Yaw)) * HMM_CosF(HMM_ToRadians(Camera.Pitch)), 
                                                                                   HMM_SinF(HMM_ToRadians(Camera.Pitch)), 
                                                                                   HMM_SinF(HMM_ToRadians(Camera.Yaw)) * HMM_CosF(HMM_ToRadians(Camera.Pitch))));
}

void
UpdateMovement()
{
    hmm_vec3 FrontScaled = HMM_MultiplyVec3f(Camera.Front, CameraSpeed);
    hmm_vec3 UpScaled = HMM_MultiplyVec3f(Camera.Up, CameraSpeed);
    
    hmm_vec3 Cross = HMM_Cross(Camera.Front, Camera.Up);
    Cross = HMM_NormalizeVec3(Cross);
    Cross = HMM_MultiplyVec3f(Cross, CameraSpeed);
    
    if(IsInputTypeDown(Types.MOVE_FORWARD)) Camera.Position = HMM_AddVec3(Camera.Position, FrontScaled);
    if(IsInputTypeDown(Types.MOVE_BACKWARD)) Camera.Position = HMM_SubtractVec3(Camera.Position, FrontScaled);
    if(IsInputTypeDown(Types.MOVE_LEFT)) Camera.Position = HMM_AddVec3(Camera.Position, Cross);
    if(IsInputTypeDown(Types.MOVE_RIGHT)) Camera.Position = HMM_SubtractVec3(Camera.Position, Cross);
    if(IsInputTypeDown(Types.MOVE_UP)) Camera.Position = HMM_SubtractVec3(Camera.Position, UpScaled);
    if(IsInputTypeDown(Types.MOVE_DOWN)) Camera.Position = HMM_AddVec3(Camera.Position, UpScaled);
    
    Camera.Target = HMM_AddVec3(Camera.Position, Camera.Front);
    
    #if 0
    int X = (int)floorf(Camera.Position.X)%World.ChunkSize;
    int Y = (int)floorf(Camera.Position.Y)%World.ChunkSize;
    int Z = (int)floorf(Camera.Position.Z)%World.ChunkSize;
    
    if(World.PlayerPosition.X != X || World.PlayerPosition.Y != Y || World.PlayerPosition.Z != Z)
    {
        UpdatePlayerPosition(&World, (position){X, Y, Z});
    }
    #endif
}

static void 
PlatformLayer_Update(float dt, struct rf_platform_event *Events, unsigned int EventCount)
{
    static bool left, right, forward, back, up, down;
    for(int Index = 0; Index < EventCount; ++Index)
    {
        rf_platform_event Event = Events[Index];
        switch(Event.Type)
        {
            case RF_EVENT_TYPE_KEYBOARD:
            {
                rf_platform_keyboard_event_info Keyboard = Event.KeyboardInfo;
                int Keycode = Keyboard.KeyCode;
                UpdateKeycode(Keycode, Keyboard.IsDown);
            } break;
            
            case RF_EVENT_TYPE_MOUSE:
            {
                rf_platform_mouse_event_info MouseInfo = Event.MouseInfo;
                if(MouseInfo.Type == MOUSE_EVENT_MOVE)
                {
                    if(!DebugMode)
                    {
                        int CenterMouseX = RfPlatform.GetWindowWidth() / 2;
                        int CenterMouseY = RfPlatform.GetWindowHeight() / 2;
                        
                        int MouseXDifference = MouseInfo.MouseX-CenterMouseX;
                        int MouseYDifference = MouseInfo.MouseY-CenterMouseY;
                        
                        if(MouseXDifference != 0 || MouseYDifference != 0)
                        {
                            UpdateCamera(MouseXDifference*Sensitivity, MouseYDifference*Sensitivity);
                        }                        
                    }
                }
            } break;
            
            case RF_EVENT_TYPE_GAMEPAD:
            {
                if(Event.GamepadInfo.Type == RF_PLATFORM_GAMEPAD_EVENT_TYPE_BUTTON)
                {
                    if(Event.GamepadInfo.Button == RF_PLATFORM_GAMEPAD_A && Event.GamepadInfo.IsDown)
                    {
                        RfPlatform.Log("A button down\n");
                    }
                    else if(Event.GamepadInfo.Button == RF_PLATFORM_GAMEPAD_A && !Event.GamepadInfo.IsDown)
                    {
                        RfPlatform.Log("A button NOT down\n");
                    }
                }
            } break;
            
            case RF_EVENT_TYPE_RESIZE:
            {
                
            } break;
        }
    }
    
    
    
    static bool DebugWasDown;
    if(IsInputTypeDown(Types.DEBUG_MODE))        
    {       
        if(!DebugWasDown)
        {                
            DebugMode = !DebugMode;
            
            if(DebugMode)
            {            
                RfPlatform.SetMouseMode(RF_MOUSE_MODE_APPLICATION);
                RfPlatform.SetShouldDrawIMGUI(true);
            }
            else
            {
                RfPlatform.SetMouseMode(RF_MOUSE_MODE_GAME);
                RfPlatform.SetShouldDrawIMGUI(false);
            }
            
            DebugWasDown = true;
        }
        else
        {

        }
        
    }
    else
    {
        DebugWasDown = false;
    }

    
    
    if(!DebugMode)
    {
        UpdateMovement();
    }       
    
#if DEAR_IMGUI 
    
    #endif

}

static void 
PlatformLayer_Render()
{
    DeviceContext->lpVtbl->OMSetRenderTargets(DeviceContext, 1, RenderTargetView, *DepthStencilView);
    float colors[4] = {0.0f, 0.5f, 0.5, 1.0f};
    DeviceContext->lpVtbl->ClearRenderTargetView(DeviceContext, *RenderTargetView, colors);
    UINT Flags = 0;
    Flags |= D3D11_CLEAR_DEPTH;
    Flags |= D3D11_CLEAR_STENCIL;
    DeviceContext->lpVtbl->ClearDepthStencilView(DeviceContext, *DepthStencilView, Flags, 1.0f, 0);
    hmm_mat4 WorldMat4 = HMM_Mat4d(1.0f);
    hmm_mat4 View = HMM_LookAtLH(Camera.Position, Camera.Target, Camera.Up);
    hmm_mat4 Project = HMM_PerspectiveLH(70.0f, 1280.0f/720.0f, 0.1f, 10000.0f); 
    hmm_mat4 WorldViewProjection = HMM_MultiplyMat4(WorldMat4, HMM_MultiplyMat4(View, Project));
    u16 ChunkCount = World.ChunkCount;
#if HEIGHTMAP 
    constant_buffer ShaderBuffer = {WorldViewProjection, HMM_Vec4(0.0, 0.0, 0.0, 255)};
    DeviceContext->lpVtbl->UpdateSubresource(DeviceContext, ConstantBuffer, 0, 0, &ShaderBuffer, 0, 0);
    DeviceContext->lpVtbl->VSSetConstantBuffers(DeviceContext, 0, 1, &ConstantBuffer);
    DeviceContext->lpVtbl->Draw(DeviceContext, HeightmapVertexCount, 0);
#else
    int FinalSize = 0;
    for(int i = 0; i < ChunkCount*ChunkCount; i++)
    {
        int ChunkX = i%ChunkCount;
        int ChunkY = 0;
        int ChunkZ = ((i/ChunkCount)%ChunkCount);
        hmm_vec4 ChunkPosition = HMM_Vec4(ChunkX, ChunkY, ChunkZ, World.ChunkSize);
        constant_buffer ShaderBuffer = {WorldViewProjection, ChunkPosition};
        DeviceContext->lpVtbl->UpdateSubresource(DeviceContext, (ID3D11Resource *)ConstantBuffer, 0, 0, &ShaderBuffer, 0, 0);
        DeviceContext->lpVtbl->VSSetConstantBuffers(DeviceContext, 0, 1, &ConstantBuffer);
        
        UINT Stride = sizeof(vertex);
        UINT Offset = 0;
        chunk_info ChunkInfo = *(World.ChunkInfo + i);
        DeviceContext->lpVtbl->IASetVertexBuffers(DeviceContext, 0, 1, &ChunkInfo.VertexBuffer, &Stride, &Offset);
        DeviceContext->lpVtbl->Draw(DeviceContext, ChunkInfo.VertexCount, 0);
        FinalSize += ChunkInfo.VertexCount;
    }
#endif
}

static void 
PlatformLayer_AudioCallback(rf_platform_audio *Audio)
{
    
}

PL_DLL_EXPORT rf_platform_layer_app_description 
PlatformLayer_EntryPoint(int ArgCount, char **ArgV)
{
    rf_platform_layer_app_description OurAppDescription = {0};
    OurAppDescription.Init = PlatformLayer_Init;
    OurAppDescription.Shutdown = PlatformLayer_Shutdown;
    OurAppDescription.Update = PlatformLayer_Update;
    OurAppDescription.Render = PlatformLayer_Render;
    OurAppDescription.AudioCallback = PlatformLayer_AudioCallback;
    OurAppDescription.WindowWidth = 1280; //1280*720
    OurAppDescription.WindowHeight = 720;
    OurAppDescription.IsFullscreen = 0;
    OurAppDescription.NeedsDepthBuffer = 1;
    OurAppDescription.NeedsStencilBuffer = 1;
    OurAppDescription.ShouldVSync = 1;
    OurAppDescription.WindowTitle = "Voxels";     
    OurAppDescription.NeedsAudio = 0;
    OurAppDescription.NeedsNetworking = 0;
    
    InputInitialization();
    
    return(OurAppDescription);
}
