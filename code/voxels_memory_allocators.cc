#include "memory_types.h"

void 
PoolInit(pool_arena *Arena, void *BackingBuffer, size_t Stride, size_t BufferLength)
{
    Arena->Buffer = (u8 *)BackingBuffer;
    Arena->Stride = Stride;
    Arena->BufferLength = BufferLength;
    Arena->Offset = 0;
}

inline size_t
GetCurrentPool(pool_arena Arena)
{
    return Arena.offset/Arena.stride;
}



void 
*GetPool(pool_arena *Arena, size_t Pool)
{
    
    if(Pool*Arena->Stride < Arena->BufferLength)
    {
        return Arena->Buffer+(Pool*Arena->Stride);
    }
    else
    {
        // Log
    }
    return Arena->buf;
}

void
SetPoolOffset(pool_arena *Arena, size_t Pool)
{
    if(Pool*Arena->Stride < Arena->BufferLength)
    {
        Arena->Offset = Pool*Arena->Stride;
    }
    else
    {
        // Log
    }
}