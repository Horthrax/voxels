// Copyright Rageface Studios, LTD. All Rights Reserved.

#ifndef VOXELS_TYPES_H
#define VOXELS_TYPES_H

#include <stdint.h>

typedef uint8_t u8;
typedef uint16_t u16;
typedef uint32_t u32;
typedef uint64_t u64;
typedef __uint128_t u128;

typedef int8_t s8;
typedef int16_t s16;
typedef int32_t s32;
typedef int64_t s64;

typedef int32_t b32;

typedef float f32;
typedef double f64;

typedef uintptr_t memory_index;

#define Assert(Expression) if(!(Expression)) { *(volatile int *)0 = 0; }
#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

#endif