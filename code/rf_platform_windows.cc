// TODO(zak): rf_platform is mostly complete but should probably not be used to ship games yet!
// Here's a short list of things that need to be done, before this shipst
// - Non-Xinput Gamepad Support and Hot Plugging
// - Sync and ASync file io (Use IOCP with existing thread pool CreateFile is SLOOOWWW) 
// - Add Ability for MSAA

#define WIN32_LEAN_AND_MEAN
#include <windows.h>
#undef LoadImage // NOTE(zak): We have a function called LoadImage ourself. We don't want the windows one we so we can just undef it here

#include <winsock.h>
#include <shellapi.h>
#include <hidsdi.h>
#include <intrin.h>
#include <wincodec.h>

#define CINTERFACE
#define COBJMACROS
#define D3D11_NO_HELPERS

#include <d3d11.h>
#include <dxgi.h>

#include <initguid.h>
#include <mmdeviceapi.h>
#include <audioclient.h>

#include <xinput.h>

#include <xmmintrin.h>

#if DEAR_IMGUI
#include "imgui_impl_win32.h"
#include "imgui_impl_dx11.h"
#endif

// NOTE(zak): mmdeviceapi.h does not define these for us. They are simply marked as extern, and i couldn't find a lib 
// to that would cause these to exist. In this case we will just define them outselves
DEFINE_GUID(CLSID_MMDeviceEnumerator, 0xBCDE0395, 0xE52F, 0x467C, 0x8E, 0x3D, 0xC4, 0x57, 0x92, 0x91, 0x69, 0x2E);
DEFINE_GUID(IID_IMMDeviceEnumerator, 0xA95664D2, 0x9614, 0x4F35, 0xA7, 0x46, 0xDE, 0x8D, 0xB6, 0x36, 0x17, 0xE6);
DEFINE_GUID(IID_IAudioClient, 0x1CB9AD4C, 0xDBFA, 0x4C32, 0xB1, 0x78, 0xC2, 0xF5, 0x68, 0xA7, 0x03, 0xB2);
DEFINE_GUID(IID_IAudioRenderClient, 0xF294ACFC, 0x3146, 0x4483, 0xA7, 0xBF, 0xAD, 0xDC, 0xA7, 0xC2, 0x60, 0xE2);
DEFINE_GUID(IID_IMMNotificationClient, 0x7991EEC9, 0x7E89, 0x4D85, 0x83, 0x90, 0x6C, 0x70, 0x3C, 0xEC, 0x60, 0xC0);

static rf_b32 GlobalRunning; 
static rf_f64 GlobalPerfCountFrequency; 
static rf_b32 GlobalIsWindowActive;
static rf_b32 D3DInitalized;
static rf_b32 UpdateXInputControllerList;

typedef struct d3d11_core
{    
    ID3D11Device *Device;
    ID3D11DeviceContext *DeviceContext;
    IDXGISwapChain *Swapchain;
    ID3D11RenderTargetView *RenderTargetView;
    ID3D11DepthStencilView *DepthStencilView;
    ID3D11DepthStencilState *DepthStencilState;
    ID3D11RasterizerState *RasterizerState;
    
    ID3D11RenderTargetView **StableRenderTargetView;
    ID3D11DepthStencilView **StableDepthStencilView;
    
    D3D_FEATURE_LEVEL HighestSupportedFeatureLevel;    
} d3d11_core;

static d3d11_core D3D11Core;

static rf_platform_layer_app_description AppDescription;
static XINPUT_STATE PreviousState;

static void *Win32MainFiber;
static void *Win32MessageFiber;

// NOTE(zak): These  need to be global or malloc'd/new'd WASAPI will use a pointer 
// to these later on to call the functions you bind to it via lpVtbl therefore having
// it as a local variable that gets cleaned up doesnt work. 
//tldr: Conforming with C++ com shit even though were in C 
static IMMNotificationClient NotificationClient;
static IMMNotificationClientVtbl NotificationClientVTable;

static HANDLE AudioThreadHandle;

static bool ShouldReInitAudio;
static bool ShouldSetMouse;

#define MAX_EVENTS 1024

static rf_platform_event Events[MAX_EVENTS];
static rf_u32 CurrentEvent;

#define LOG_FILE_NAME "log.txt"

typedef DWORD xinput_get_state(DWORD dwUserIndex, XINPUT_STATE *pState);
static xinput_get_state *XInputGetState_;
#define XInputGetState XInputGetState_

typedef DWORD xinput_set_state(DWORD dwUserIndex, XINPUT_VIBRATION *pVibration);
static xinput_set_state *XInputSetState_;
#define XInputSetState XInputSetState_

#define MAX_ADAPTER_COUNT 10

static rf_u32 NumControllerConnected;

static rf_u32 WindowWidth;
static rf_u32 WindowHeight;
static rf_mouse_mode MouseMode;
static rf_b32 BetweenShouldUpdateAndDrawIMGUI;

// TODO(zak): Make sure "x" isn't null before we release it?????
#define SAFE_RELEASE(x) \
x->lpVtbl->Release(x);\
x = 0;

#define NVIDIA_VENDOR_ID 0x10DE
#define AMD_VENDOR_ID 0x1002

typedef struct win32_audio
{
    IAudioClient *AudioClient;
    IAudioRenderClient *AudioRenderClient;    
    rf_u32 SamplesPerSecond;
    rf_u32 ChannelCount;
} win32_audio; 

typedef struct win32_game
{   
    // NOTE(zak): Only used if the game is currently in a DLL
    // and not apart of the EXE
    HMODULE GameDLL;
    
    rf_platform_layer_entrypoint *Entrypoint;
    FILETIME LastWriteTime;
    
    rf_b32 Loaded;
} win32_game;

typedef struct win32_state
{
    char EXEFileName[MAX_PATH];
    char *OnePastLastEXEFileNameSlash;  
} win32_state;

typedef struct rf_platform_file_handle
{
    HANDLE Win32FileHandle;
} rf_platform_file_handle;

#pragma function(memset)
void *memset(void *dest, int c, size_t count)
{
    char *bytes = (char *)dest;
    while (count--)
    {
        *bytes++ = (char)c;
    }
    return dest;
}

#pragma function(memcmp)
int memcmp(void const *ptr1, void const *ptr2, size_t count)
{
    unsigned char *s1 = (unsigned char*)ptr1;
    unsigned char *s2 = (unsigned char*)ptr2;
    
    while (count-- > 0)
    {
        if (*s1++ != *s2++) 
        {
            return s1[-1] < s2[-1] ? -1 : 1;
        }
    }
    
    return 0;    
}

static inline rf_s32
RoundReal32ToInt32(rf_f32 Real32)
{
    rf_s32 Result = _mm_cvtss_si32(_mm_set_ss(Real32));
    return(Result);
}


#define Kilobytes(Value) ((Value)*1024LL)

static void *
Win32AllocateMemory(size_t Size)
{
    void *Result = 0;
    
    if(Size < Kilobytes(4))
    {
        Result = HeapAlloc(GetProcessHeap(), 0, Size);
    }
    else
    {
        Result = VirtualAlloc(0, Size, MEM_COMMIT|MEM_RESERVE, PAGE_READWRITE);
    }
    
    return(Result);
}

#undef Kilobytes

static void
Win32FreeMemory(void *Buffer)
{
    if(Buffer)
    {
        VirtualFree(Buffer, 0, MEM_RELEASE);
    }
}

static void *
Win32GetD3DDevice()
{
    RfPlatform_Assert(D3D11Core.Device != 0);
    return(D3D11Core.Device);
}

static void *
Win32GetD3DDeviceContext()
{
    RfPlatform_Assert(Device != 0);
    return(D3D11Core.DeviceContext);    
}

static void *
Win32GetD3DRenderTargetView()
{
    RfPlatform_Assert(D3D11Core.StableRenderTargetView != 0);
    return(D3D11Core.StableRenderTargetView);
}

static void *
Win32GetD3DDepthStencilView()
{
    RfPlatform_Assert(D3D11Core.StableDepthStencilView != 0);
    return(D3D11Core.StableDepthStencilView);
}

static rf_u32
Win32GetD3DFeatureLevel()
{
    RfPlatform_Assert(D3D11Core.HighestSupportedFeatureLevel != 0);    
    return(D3D11Core.HighestSupportedFeatureLevel);
}

static char *
Win32GetLastErrorAsString(void)
{
    char *String = 0;
    
    DWORD ErrorID = GetLastError();
    if(ErrorID != 0)
    {
        FormatMessageA(FORMAT_MESSAGE_ALLOCATE_BUFFER | FORMAT_MESSAGE_FROM_SYSTEM | FORMAT_MESSAGE_IGNORE_INSERTS,
                       0, ErrorID, MAKELANGID(LANG_NEUTRAL, SUBLANG_DEFAULT), (LPSTR)&String, 0, 0);                        
    }
    
    return(String);
}

static rf_file
Win32OpenFile(char *FileName)
{
    rf_file Result = {0};
    
    Result.Handle = (rf_platform_file_handle *)Win32AllocateMemory(sizeof(rf_platform_file_handle));
    if(Result.Handle)
    {
        Result.Handle->Win32FileHandle = CreateFileA(FileName, GENERIC_READ, FILE_SHARE_READ, 0 ,OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, 0);
        if(Result.Handle->Win32FileHandle != INVALID_HANDLE_VALUE)
        {
            Result.IsValid = true;
        }
        else
        {
            RF_LOG("CreateFileA failed for file %s with error: %s", FileName, Win32GetLastErrorAsString());
        }
    }
    else
    {
        RF_LOG("Win32AllocateMemory failed to allocate handle for file %s with error: %s", FileName, Win32GetLastErrorAsString());
    }
    
    return(Result);
}

static size_t
Win32GetFileSize(rf_file *File)
{
    size_t Result = 0;
    
    LARGE_INTEGER FileSize;
    if(GetFileSizeEx(File->Handle->Win32FileHandle, &FileSize))
    {
        Result = FileSize.QuadPart;
    }
    
    return(Result);
}

static rf_b32
Win32ReadFile(rf_file *File, void *Buffer, size_t BytesToRead)
{
    rf_b32 Result = false;
    
    DWORD BytesRead;
    if(ReadFile(File->Handle->Win32FileHandle, Buffer, (DWORD)BytesToRead, &BytesRead, 0) &&  (DWORD)BytesToRead == BytesRead)
    {
        Result = true;
    }
    else
    {
        RF_LOG("ReadFile failed with error: %s", Win32GetLastErrorAsString());
    }
    
    return(Result);
}

static rf_b32
Win32WriteFile(rf_file *File, void *Buffer, size_t BufferLength)
{
    rf_b32 Result = false;
    
    DWORD BytesWritten;
    if(WriteFile(File->Handle->Win32FileHandle, Buffer, (DWORD)BufferLength, &BytesWritten, 0) && BufferLength == BytesWritten)
    {
        Result = true;
    }
    
    return(Result);
}


static rf_u32
Win32GetWindowWidth()
{
    return WindowWidth;
}

static rf_u32
Win32GetWindowHeight()
{
    return WindowHeight;
}

static void 
Win32SetMouseMode(rf_mouse_mode NewMouseMode)
{
    // if were going from application to game mode
    if(MouseMode == RF_MOUSE_MODE_APPLICATION && NewMouseMode == RF_MOUSE_MODE_GAME)
    {
        RECT WindowRect = {0};
        
        // TODO(zak): This includes the borders, it probably shouldn't just incase
        // FIXME BEFORE SHIPPING
        GetWindowRect(GetActiveWindow(), &WindowRect);
        ClipCursor(&WindowRect);
        ShowCursor(0);
    }
    
    // If we're going from game to application mode
    if(MouseMode == RF_MOUSE_MODE_GAME && NewMouseMode == RF_MOUSE_MODE_APPLICATION)
    {
        ClipCursor(0);
        ShowCursor(1);
    }    
    
    MouseMode = NewMouseMode;
}

// Returns a RGBA image. No matter what you pass it in
static rf_platform_image
Win32LoadImage(char *FileName)
{
    rf_platform_image Result = {};
    
    static IWICImagingFactory* WICFactory;
    if(!WICFactory)
    {
        if (CoCreateInstance(CLSID_WICImagingFactory, 0, CLSCTX_INPROC_SERVER, IID_IWICImagingFactory, (void **)&WICFactory) != S_OK)            
        {
            RF_LOG("CoCreateInstance failed with IID_IWICImagingFactory: %s", Win32GetLastErrorAsString());
        }            
    }
    
    // Convert char to wchar
    int Size = MultiByteToWideChar(CP_ACP, 0, FileName, -1, NULL, 0);
    WCHAR *WideFileName = (WCHAR *)HeapAlloc(GetProcessHeap(), 0, Size);
    if(WideFileName)
    {
        MultiByteToWideChar(CP_ACP, 0, FileName, -1, (LPWSTR)WideFileName, Size);        
        IWICBitmapDecoder* Decoder = 0;
        if (WICFactory->lpVtbl->CreateDecoderFromFilename(WICFactory, WideFileName, 0, GENERIC_READ, WICDecodeMetadataCacheOnDemand, &Decoder) == S_OK)
        {
            IWICBitmapFrameDecode* Frame;
            if(Decoder->lpVtbl->GetFrame(Decoder, 0, &Frame) == S_OK)
            {
                IWICBitmapSource* RGBAImageFrame;
                if (WICConvertBitmapSource(GUID_WICPixelFormat32bppRGBA, (IWICBitmapSource *)Frame, &RGBAImageFrame) == 0)
                {
                    UINT ImageWidth;
                    UINT ImageHeight;
                    UINT Depth = 4;
                    if (RGBAImageFrame->lpVtbl->GetSize(RGBAImageFrame, &ImageWidth, &ImageHeight) == 0)
                    {
                        void *ImageData = HeapAlloc(GetProcessHeap(), 0, ImageWidth * ImageHeight * Depth);
                        if(ImageData)
                        {
                            if(RGBAImageFrame->lpVtbl->CopyPixels(RGBAImageFrame, 0, ImageWidth * Depth, ImageWidth * ImageHeight * Depth, (BYTE *)ImageData) == S_OK)
                            {
                                Result.Width = ImageWidth;
                                Result.Height = ImageHeight;
                                Result.Data = ImageData;
                            }
                            else
                            {
                                RF_LOG("CopyPixels failed on file: %s : %s", FileName, Win32GetLastErrorAsString());
                                HeapFree(GetProcessHeap(), 0, ImageData);
                            }
                        }
                        else
                        {
                            RF_LOG("HeapAlloc failed to allocate image data for file: %s : %s", FileName, Win32GetLastErrorAsString());
                        }
                    }
                    else
                    {
                        RF_LOG("GetSize failed to get image size for file: %s : %s", FileName, Win32GetLastErrorAsString());
                    }
                    
                    RGBAImageFrame->lpVtbl->Release(RGBAImageFrame);
                }
                else
                {
                    RF_LOG("WICConvertBitmapSource failed to convert image data for file: %s : %s", FileName, Win32GetLastErrorAsString());
                }
                
                Frame->lpVtbl->Release(Frame);
            }
            else
            {
                RF_LOG("GetFrame failed for file: %s : %s", FileName, Win32GetLastErrorAsString());                
            }
            
            Decoder->lpVtbl->Release(Decoder);
        }
        else
        {
            RF_LOG("CreateDecoderFromFilename failed for file: %s : %s", FileName, Win32GetLastErrorAsString());
        }
        
        HeapFree(GetProcessHeap(), 0, WideFileName);
    }
    
    return(Result);
}

static void
Win32SetShouldDrawIMGUI(bool ShouldDrawImgui)
{
    BetweenShouldUpdateAndDrawIMGUI = ShouldDrawImgui;
}

static inline rf_u32
CStringLength(char* String)
{
    rf_u32 Count = 0;
    while(*String++)
    {
        ++Count;
    }
    return (Count);
}

static void 
Win32Log(char* String)
{
    if (IsDebuggerPresent())
    {
        OutputDebugStringA(String);
    }
    
    static HANDLE LogFilehandle;
    if(!LogFilehandle)
    {
        LogFilehandle = CreateFileA(LOG_FILE_NAME, GENERIC_WRITE, FILE_SHARE_WRITE, 0, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0);
        if(LogFilehandle == INVALID_HANDLE_VALUE)
        {
            // DO SOMETHING WE COULDNT MAKE A LOG FILE
        }
    }
    
    if(LogFilehandle != INVALID_HANDLE_VALUE)
    {
        DWORD BytesWritten = 0;
        if(!WriteFile(LogFilehandle, String, CStringLength(String), &BytesWritten, 0) || BytesWritten == CStringLength(String))
        {
            // We couldn't write to the log file
        }
    }
}

static inline LARGE_INTEGER
Win32GetWallClock(void)
{
    LARGE_INTEGER Result;
    QueryPerformanceCounter(&Result);
    return(Result);
}

static inline rf_f32
Win32GetSecondsElapsed(LARGE_INTEGER Start, LARGE_INTEGER End)
{
    rf_f32 Result = ((rf_f32)(End.QuadPart - Start.QuadPart) /
                     (rf_f32)GlobalPerfCountFrequency);
    return(Result);
}

static rf_u32
Win32GetOSVersion(void)
{
    int *x = ((int **) __readgsqword(0x30))[12];               
    return(x[0x46]);
}

static FILETIME
Win32GetLastWriteTime(char *File)
{
    FILETIME Result = {0};
    WIN32_FILE_ATTRIBUTE_DATA FileData;
    
    if(GetFileAttributesExA(File, GetFileExInfoStandard, &FileData))
    {        
        Result = FileData.ftLastWriteTime;
    }
    else
    {
        RF_LOG("GetFileAttributesExA with file %s failed with error: %s", File, Win32GetLastErrorAsString());
    }
    
    return (Result);
}

static rf_b32
Win32InitD3D11(HWND Window)
{
    rf_b32 Result = false;    
    
    UINT Flags = 0;
#if DEBUG
    Flags |= D3D11_CREATE_DEVICE_DEBUG;
#endif    
    
    D3D_FEATURE_LEVEL FeatureLevels[] =
    {
        D3D_FEATURE_LEVEL_11_0,
        D3D_FEATURE_LEVEL_10_1,
        D3D_FEATURE_LEVEL_10_0,
    };    
    
    // NOTE(zak): We will only use this factory for enumerating through adapters on the system
    // then we will use the adapter from the device for creating the swapchain later on 
    IDXGIFactory1 *TempFactory = 0;
    if(CreateDXGIFactory1(IID_IDXGIFactory, (void **)&TempFactory) == 0)
    {                
        IDXGIAdapter1 *Adapters[MAX_ADAPTER_COUNT] = {0};
        
        int AdapterCount = 0;       
        IDXGIAdapter1 *Adapter;
        while(TempFactory->lpVtbl->EnumAdapters1(TempFactory, AdapterCount, &Adapter) != DXGI_ERROR_NOT_FOUND)
        {
            Adapters[AdapterCount++] = Adapter;
        }                
        SAFE_RELEASE(TempFactory);
        
        int IndexOfHighestEndGPU = 0;
        size_t HighestVideoMemory = 0;
        bool DoesHaveHighEndAdapter = 0;
        for(int Index = 0; Index < AdapterCount; ++Index)
        {
            
            DXGI_ADAPTER_DESC1 AdapterDescription = {0};
            Adapters[Index]->lpVtbl->GetDesc1(Adapters[Index], &AdapterDescription);
            
            // TODO(zak): Maybe collect "low-end" adapters at some point
            // NOTE(zak): We prefer AMD and Nvidia gpus over intel gpus
            if((AdapterDescription.VendorId == NVIDIA_VENDOR_ID || AdapterDescription.VendorId == AMD_VENDOR_ID)  && !AdapterDescription.Flags)
            {                                
                if(AdapterDescription.DedicatedVideoMemory > HighestVideoMemory)
                {
                    HighestVideoMemory = AdapterDescription.DedicatedVideoMemory;
                    IndexOfHighestEndGPU = Index;
                    DoesHaveHighEndAdapter = true;
                }
            }
        }
        
        // NOTE(zak): This person doesn't have a high end nvidia or amd gpu in this system. So 
        // lets just use the default system adapter            
        IDXGIAdapter1 *AdapterToUse = 0;
        if(!DoesHaveHighEndAdapter)
        {
            AdapterToUse = Adapters[0];
        }
        else
        {
            AdapterToUse = Adapters[IndexOfHighestEndGPU];
        }

        if(D3D11CreateDevice((IDXGIAdapter *)AdapterToUse,
                             D3D_DRIVER_TYPE_UNKNOWN,
                             0,
                             Flags,
                             FeatureLevels,
                             ArrayCount(FeatureLevels),
                             D3D11_SDK_VERSION,
                             &D3D11Core.Device,
                             &D3D11Core.HighestSupportedFeatureLevel,
                             &D3D11Core.DeviceContext) != S_OK)
        {
            DXGI_ADAPTER_DESC1 AdapterDescription = {0};
            AdapterToUse->lpVtbl->GetDesc1(AdapterToUse, &AdapterDescription);
            // TODO(zak): I dont know if %ls will work here lets check this            
            RF_LOG("D3D11CreateDevice failed with driver type D3D_DRIVER_TYPE_UNKNOWN using Adapter %ls. Trying default adapter with  D3D_DRIVER_TYPE_HARDWARE error: %s", AdapterDescription.Description, Win32GetLastErrorAsString());
            
            SAFE_RELEASE(AdapterToUse);
            
            if(D3D11CreateDevice(0,
                              D3D_DRIVER_TYPE_HARDWARE,
                              0,
                              Flags,
                              FeatureLevels,
                              ArrayCount(FeatureLevels),
                              D3D11_SDK_VERSION,
                              &D3D11Core.Device,
                                 &D3D11Core.HighestSupportedFeatureLevel,
                              &D3D11Core.DeviceContext) != S_OK)
            {                                
                RF_LOG("D3D11CreateDevice failed with driver type D3D_DRIVER_TYPE_HARDWARE using the default adapter. Trying D3D_DRIVER_TYPE_WARP error: %s", Win32GetLastErrorAsString());
                
                if(D3D11CreateDevice(0,
                                     D3D_DRIVER_TYPE_WARP,
                                     0,
                                     Flags,
                                     FeatureLevels,
                                     ArrayCount(FeatureLevels),
                                     D3D11_SDK_VERSION,
                                     &D3D11Core.Device,
                                     &D3D11Core.HighestSupportedFeatureLevel,
                                     &D3D11Core.DeviceContext) != S_OK)
                {
                    RF_LOG("D3D11CreateDeviceAndSwapChain failed with driver type D3D_DRIVER_TYPE_WARP. Could not initialize D3D11: %s", Win32GetLastErrorAsString());
                }
            }
        }
        
        // Free all the adapters we have in memory

        for(int Index = 0; Index < MAX_ADAPTER_COUNT; ++Index)
        {
            if(Adapters[Index])
            {
                SAFE_RELEASE(Adapters[Index]);
            }
        }                
        
        if(D3D11Core.Device && D3D11Core.DeviceContext)
        {
            IDXGIDevice *DXGIDevice = 0;
            if(D3D11Core.Device->lpVtbl->QueryInterface(D3D11Core.Device, IID_IDXGIDevice, (void **)&DXGIDevice) == 0)
            {
                IDXGIAdapter *DXGIAdapter = 0;
                if(DXGIDevice->lpVtbl->GetAdapter(DXGIDevice, &DXGIAdapter) == 0)
                {
                    SAFE_RELEASE(DXGIDevice);
                    
                    IDXGIFactory *DXGIFactory = 0;
                    if(DXGIAdapter->lpVtbl->GetParent(DXGIAdapter, IID_IDXGIFactory, (void **)&DXGIFactory) == 0)
                    {                                        
                        SAFE_RELEASE(DXGIAdapter);
                        
                        rf_b32 bFlipModel = false;
                        DXGI_SWAP_EFFECT SwapEffect = DXGI_SWAP_EFFECT_DISCARD;
                        if(Win32GetOSVersion() == 10)
                        {
                            bFlipModel = true;
                            SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
                        }
                        else if(Win32GetOSVersion() == 8)
                        {
                            bFlipModel = true;
                            SwapEffect = DXGI_SWAP_EFFECT_FLIP_SEQUENTIAL;
                        }
                        
                        RECT WindowRect;
                        GetWindowRect(Window, &WindowRect);
                        
                        DEVMODEA DevMode;
                        EnumDisplaySettingsA(0, ENUM_CURRENT_SETTINGS, &DevMode);
                        
                        int BackBufferWidth = AppDescription.WindowWidth;
                        int BackBufferHeight = AppDescription.WindowHeight;
                        if(AppDescription.IsFullscreen)
                        {
                            BackBufferWidth = GetSystemMetrics(SM_CXSCREEN);
                            BackBufferHeight = GetSystemMetrics(SM_CYSCREEN);
                        }
                        
                        DXGI_SWAP_CHAIN_DESC SwapChainDescription = {0};
                        SwapChainDescription.BufferDesc.Width = BackBufferWidth;
                        SwapChainDescription.BufferDesc.Height = BackBufferHeight;
                        SwapChainDescription.BufferDesc.RefreshRate.Numerator = DevMode.dmDisplayFrequency > 1 ? DevMode.dmDisplayFrequency : 60;
                        SwapChainDescription.BufferDesc.RefreshRate.Denominator = 1;
                        SwapChainDescription.BufferDesc.Format = DXGI_FORMAT_B8G8R8A8_UNORM;
                        SwapChainDescription.BufferDesc.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
                        SwapChainDescription.BufferDesc.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
                        
                        // NOTE(zak): Change this later on if we want MSAA
                        SwapChainDescription.SampleDesc.Count = 1;
                        SwapChainDescription.SampleDesc.Quality = 0;
                        
                        SwapChainDescription.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
                        SwapChainDescription.BufferCount = bFlipModel ? 2 : 1; // NOTE(zak): BufferCount - 1 is supposidly correct for double buffering with DXGI_SWAP_EFFECT_DISCARD. 
                        SwapChainDescription.OutputWindow = Window;
                        SwapChainDescription.Windowed = AppDescription.IsFullscreen ? 0 : 1;
                        SwapChainDescription.SwapEffect = bFlipModel ? DXGI_SWAP_EFFECT_FLIP_DISCARD : DXGI_SWAP_EFFECT_DISCARD;
                        SwapChainDescription.Flags =  0;
                        
                        
                        if(DXGIFactory->lpVtbl->CreateSwapChain(DXGIFactory, 
                                                                (IUnknown *)D3D11Core.Device,
                                                                &SwapChainDescription,
                                                                &D3D11Core.Swapchain) == 0)
                        {
                            SAFE_RELEASE(DXGIFactory);                                         
                            
                            ID3D11Texture2D *BackBuffer;
                            if(D3D11Core.Swapchain->lpVtbl->GetBuffer(D3D11Core.Swapchain, 0, IID_ID3D11Texture2D, (void **)&BackBuffer) == S_OK)
                            {            
                                D3D11_RENDER_TARGET_VIEW_DESC RenderTargetViewDescription = {};
                                RenderTargetViewDescription.Format = DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;
                                RenderTargetViewDescription.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
                                RenderTargetViewDescription.Texture2D.MipSlice = 0;
                                
                                if(D3D11Core.Device->lpVtbl->CreateRenderTargetView(D3D11Core.Device, (ID3D11Resource * )BackBuffer, &RenderTargetViewDescription, &D3D11Core.RenderTargetView) == S_OK)
                                {
                                    D3D11Core.StableRenderTargetView = &D3D11Core.RenderTargetView;
                                    
                                    SAFE_RELEASE(BackBuffer);
                                    
                                    D3D11_TEXTURE2D_DESC DepthStencilBufferDesc = {0};
                                    DepthStencilBufferDesc.ArraySize = 1;
                                    DepthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
                                    DepthStencilBufferDesc.CPUAccessFlags = 0;
                                    DepthStencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
                                    DepthStencilBufferDesc.Width = SwapChainDescription.BufferDesc.Width;
                                    DepthStencilBufferDesc.Height = SwapChainDescription.BufferDesc.Height;
                                    DepthStencilBufferDesc.MipLevels = 1;
                                    DepthStencilBufferDesc.SampleDesc.Count = 1;
                                    DepthStencilBufferDesc.SampleDesc.Quality = 0;
                                    DepthStencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;
                                    
                                    ID3D11Texture2D *DepthStencilBuffer;
                                    if(D3D11Core.Device->lpVtbl->CreateTexture2D(D3D11Core.Device, &DepthStencilBufferDesc, 0, &DepthStencilBuffer) == S_OK)
                                    {
                                        if(D3D11Core.Device->lpVtbl->CreateDepthStencilView(D3D11Core.Device, (ID3D11Resource * )DepthStencilBuffer, 0, &D3D11Core.DepthStencilView) == S_OK)
                                        {
                                            D3D11Core.StableDepthStencilView = &D3D11Core.DepthStencilView;
                                            
                                            SAFE_RELEASE(DepthStencilBuffer);
                                            
                                            D3D11_DEPTH_STENCIL_DESC DepthStencilDesc = {0};
                                            DepthStencilDesc.DepthEnable = AppDescription.NeedsDepthBuffer;
                                            DepthStencilDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
                                            DepthStencilDesc.DepthFunc = D3D11_COMPARISON_LESS;
                                            DepthStencilDesc.StencilEnable = AppDescription.NeedsStencilBuffer; 
                                            DepthStencilDesc.StencilReadMask = D3D11_DEFAULT_STENCIL_READ_MASK;
                                            DepthStencilDesc.StencilWriteMask = D3D11_DEFAULT_STENCIL_WRITE_MASK;
                                            DepthStencilDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
                                            DepthStencilDesc.FrontFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;
                                            DepthStencilDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
                                            DepthStencilDesc.FrontFace.StencilFailOp =  D3D11_STENCIL_OP_KEEP;
                                            DepthStencilDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
                                            DepthStencilDesc.BackFace.StencilDepthFailOp = D3D11_STENCIL_OP_KEEP;                        
                                            DepthStencilDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
                                            DepthStencilDesc.BackFace.StencilFailOp =  D3D11_STENCIL_OP_KEEP;
                                            
                                            if(D3D11Core.Device->lpVtbl->CreateDepthStencilState(D3D11Core.Device, &DepthStencilDesc, &D3D11Core.DepthStencilState) == S_OK)
                                            {                                                        
                                                D3D11_RASTERIZER_DESC RasterizerDesc = {};
#if WIREFRAME
                                                RasterizerDesc.FillMode = D3D11_FILL_WIREFRAME;
                                                RasterizerDesc.CullMode = D3D11_CULL_NONE;
#else 
                                                RasterizerDesc.FillMode = D3D11_FILL_SOLID;
                                                RasterizerDesc.CullMode = D3D11_CULL_NONE;
                                                RasterizerDesc.FrontCounterClockwise = 0;
                                                RasterizerDesc.DepthBias = 0;
                                                RasterizerDesc.SlopeScaledDepthBias = 0.0f;
                                                RasterizerDesc.DepthBiasClamp = 0.0f;
                                                RasterizerDesc.DepthClipEnable = 1;
                                                RasterizerDesc.ScissorEnable = 0;
                                                RasterizerDesc.MultisampleEnable = 0;
                                                RasterizerDesc.AntialiasedLineEnable = 0;
#endif
                                                
                                                if(D3D11Core.Device->lpVtbl->CreateRasterizerState(D3D11Core.Device, &RasterizerDesc, &D3D11Core.RasterizerState) == S_OK)
                                                {   
                                                    D3D11Core.DeviceContext->lpVtbl->RSSetState(D3D11Core.DeviceContext, D3D11Core.RasterizerState);
                                                    
                                                    D3D11_VIEWPORT Viewport = {0};
                                                    Viewport.Width = AppDescription.WindowWidth;
                                                    Viewport.Height = AppDescription.WindowHeight;
                                                    Viewport.MaxDepth = 1.0f;
                                                    
                                                    D3D11Core.DeviceContext->lpVtbl->RSSetViewports(D3D11Core.DeviceContext, 1, &Viewport);                
                                                    Result = true;
                                                }
                                                else
                                                {
                                                    RF_LOG("CreateRasterizerState failed with error: %s", Win32GetLastErrorAsString());
                                                } 
                                            }
                                            else
                                            {
                                                RF_LOG("CreateDepthStencilState failed with error: %s", Win32GetLastErrorAsString());
                                            }
                                        }
                                        else
                                        {
                                            RF_LOG("CreateDepthStencilView failed with error: %s", Win32GetLastErrorAsString());
                                        }
                                    }
                                    else
                                    {
                                        RF_LOG("CreateTexture2D for DepthStencilBuffer failed with error: %s", Win32GetLastErrorAsString());
                                    }
                                }
                                else
                                {
                                    RF_LOG("CreateRenderTargetView failed with error: %s", Win32GetLastErrorAsString());
                                }
                            }
                            else
                            {
                                RF_LOG("GetBuffer failed to get BackBuffer from Swapchain with error: %s", Win32GetLastErrorAsString());
                            }        
                        }
                        else
                        {                            
                            RF_LOG("CreateSwapChain failed with error: %s", Win32GetLastErrorAsString());
                        }
                    }
                    else
                    {
                        RF_LOG("GetParent failed with error: %s", Win32GetLastErrorAsString());
                    }
                }
                else
                {
                    RF_LOG("GetAdapter failed with error: %s", Win32GetLastErrorAsString());
                }
            }
            else
            {
                RF_LOG("QueryInterface failed with error: %s", Win32GetLastErrorAsString());
            }
        }
    }
    else
    {
        RF_LOG("CreateDXGIFactory failed with error: %s", Win32GetLastErrorAsString());
    }
    
    return(Result);
}

static DWORD WINAPI
Win32AudioThread(void *Parameter)
{
    win32_audio *Audio = (win32_audio *)Parameter;
    if(Audio)
    {
        SetThreadPriority(GetCurrentThread(), THREAD_PRIORITY_BELOW_NORMAL); // THREAD_PRIORITY_HIGHEST
        
        HANDLE BufferReadyEvent = CreateEvent(0, 0, 0, 0);
        if(BufferReadyEvent)
        {
            if(Audio->AudioClient->lpVtbl->SetEventHandle(Audio->AudioClient, BufferReadyEvent) == S_OK)
            {
                UINT BufferSize; 
                if(Audio->AudioClient->lpVtbl->GetBufferSize(Audio->AudioClient, &BufferSize) == S_OK)
                {
                    if(Audio->AudioClient->lpVtbl->Start(Audio->AudioClient) == S_OK)
                    {
                        for(;;)
                        {
                            if(WaitForSingleObject(BufferReadyEvent, INFINITE) == WAIT_OBJECT_0)
                            {
                                UINT Padding;
                                if(Audio->AudioClient->lpVtbl->GetCurrentPadding(Audio->AudioClient, &Padding) == S_OK)
                                {
                                    UINT WriteAmount = BufferSize - Padding;
                                    if(WriteAmount > 0)
                                    {
                                        BYTE *Buffer; 
                                        if(Audio->AudioRenderClient->lpVtbl->GetBuffer(Audio->AudioRenderClient, WriteAmount, &Buffer) == S_OK)
                                        {
                                            rf_platform_audio GameAudio = {0};
                                            GameAudio.ChannelCount = Audio->ChannelCount;
                                            GameAudio.SampleRate = Audio->SamplesPerSecond;
                                            GameAudio.SampleCount = WriteAmount;
                                            GameAudio.Buffer = (rf_s16 *)Buffer;
                                            
                                            if(AppDescription.AudioCallback)
                                            {
                                                AppDescription.AudioCallback(&GameAudio);
                                            }
                                            
                                            Audio->AudioRenderClient->lpVtbl->ReleaseBuffer(Audio->AudioRenderClient, WriteAmount, 0);
                                        }
                                    }
                                }
                            }
                        }                        
                    }
                    else
                    {
                        RF_LOG("Failed to Start IAudioClient with error: %s", Win32GetLastErrorAsString());
                    }
                }
                else
                {
                    RF_LOG("Failed to GetBufferSize with error: %s", Win32GetLastErrorAsString());
                }                
            }
            else
            {
                RF_LOG("Failed to SetEventHandle with error: %s", Win32GetLastErrorAsString());
            }
        }
        else
        {
            RF_LOG("Failed to CreateEvent with error: %s", Win32GetLastErrorAsString());
        }
    }
    else
    {
        RF_LOG("Win32AudioThread didn't receive parameters: %s", Win32GetLastErrorAsString());
    }
    
    return(0);
}

// +WASAPI IMMNotificationClient Interface

// NOTE(Zak): Just incase in the future you think you can remove these, you cant.
// These are all the functions that IMMNotificationClient requires you to implement
// in C++ these are vtables but vtables doesnt exist in C so we just bind these like 
// regular old function pointer. All of these have to be filled out and NONE can be null

// COM FEELS AMAZING MAN
static HRESULT STDMETHODCALLTYPE
WASAPIQueryInterface(IMMNotificationClient *This, REFIID IID, void **Object)
{
    if (IsEqualGUID(IID_IMMNotificationClient, IID) ||
        IsEqualGUID(IID_IUnknown, IID))
    {
        *Object = (void *)This;
        return S_OK;
    } else {
        *Object = NULL;
        return E_NOINTERFACE;
    }
}

static ULONG STDMETHODCALLTYPE
WASAPIAddRef(IMMNotificationClient *This)
{
    // NOTE(zak): Dont need to handle this
    return 1;
}

static ULONG STDMETHODCALLTYPE
WASAPIRelease(IMMNotificationClient *This)
{
    // NOTE(zak): Dont need to handle this
    return 1;
}

static HRESULT STDMETHODCALLTYPE
WASAPIOnDeviceStateChanged(IMMNotificationClient *This, LPCWSTR DeviceID, DWORD NewState)
{
    // TODO(zak): Do something more proper here later on but if the state of a device 
    // changes were just gonna reinit audio for now
    
    ShouldReInitAudio = true;
    
    return S_OK;
}

static HRESULT STDMETHODCALLTYPE 
WASAPIOnDeviceAdded(IMMNotificationClient *This, LPCWSTR DeviceID)
{
    // NOTE(zak): Dont need to handle this
    return S_OK;
}

static HRESULT STDMETHODCALLTYPE 
WASAPIOnDeviceRemoved(IMMNotificationClient *This, LPCWSTR DeviceID)
{
    // NOTE(zak): Dont need to handle this
    return S_OK;
}

static HRESULT STDMETHODCALLTYPE 
WASAPIOnDefaultDeviceChanged(IMMNotificationClient *This, 
                             EDataFlow Flow, 
                             ERole Role, 
                             LPCWSTR DeviceID)
{    
    ShouldReInitAudio = true;    
    return S_OK;
}

static HRESULT STDMETHODCALLTYPE 
WASAPIOnPropertyValueChanged(IMMNotificationClient *This, LPCWSTR DeviceID, const PROPERTYKEY Key)
{
    // NOTE(zak): Dont need to handle this
    return S_OK;
}

// -WASAPI IMMNotificationClient Interface

rf_b32
Win32InitWASAPI(void)
{
    rf_b32 Result = false;
    
    IMMDeviceEnumerator *DeviceEnumerator;
    if(CoCreateInstance(CLSID_MMDeviceEnumerator, 0, CLSCTX_ALL, IID_IMMDeviceEnumerator, (void **)&DeviceEnumerator) == S_OK)
    {       
        NotificationClientVTable.QueryInterface = WASAPIQueryInterface;
        NotificationClientVTable.AddRef = WASAPIAddRef;
        NotificationClientVTable.Release = WASAPIRelease;
        NotificationClientVTable.OnDeviceStateChanged = WASAPIOnDeviceStateChanged;
        NotificationClientVTable.OnDeviceAdded = WASAPIOnDeviceAdded;
        NotificationClientVTable.OnDeviceRemoved = WASAPIOnDeviceRemoved;
        NotificationClientVTable.OnDefaultDeviceChanged = WASAPIOnDefaultDeviceChanged;
        NotificationClientVTable.OnPropertyValueChanged = WASAPIOnPropertyValueChanged;
        
        NotificationClient.lpVtbl = &NotificationClientVTable;
        if(DeviceEnumerator->lpVtbl->RegisterEndpointNotificationCallback(DeviceEnumerator, 
                                                                          &NotificationClient) != S_OK)
        {
            RF_LOG("RegisterEndpointNotificationCallback failed with error: %s", Win32GetLastErrorAsString());
        }        
        
        IMMDevice *AudioDevice;
        if(DeviceEnumerator->lpVtbl->GetDefaultAudioEndpoint(DeviceEnumerator, eRender, eConsole, &AudioDevice) == S_OK)
        {                                    
            SAFE_RELEASE(DeviceEnumerator);            
            
            IAudioClient *AudioClient;
            if(AudioDevice->lpVtbl->Activate(AudioDevice, IID_IAudioClient, CLSCTX_ALL, 0, (void **)&AudioClient) == S_OK)
            {
                WAVEFORMATEX WaveFormat = {0};
                WaveFormat.wFormatTag = WAVE_FORMAT_PCM;
                WaveFormat.nChannels = 2; 
                WaveFormat.nSamplesPerSec = 44100; 
                WaveFormat.wBitsPerSample = 16; 
                WaveFormat.nBlockAlign = (WaveFormat.nChannels * WaveFormat.wBitsPerSample) / 8; 
                WaveFormat.nAvgBytesPerSec = (WaveFormat.nSamplesPerSec * WaveFormat.nBlockAlign);                 
                
                rf_s64 SecondsToHundredNanos = 10000000;
                REFERENCE_TIME BufferDuration = (SecondsToHundredNanos / 20); // TODO(zak): Right now length is 100 nanoSeconds. Should it be less or more?
                if(AudioClient->lpVtbl->Initialize(AudioClient,
                                                   AUDCLNT_SHAREMODE_SHARED,
                                                   AUDCLNT_STREAMFLAGS_EVENTCALLBACK | AUDCLNT_STREAMFLAGS_RATEADJUST | AUDCLNT_STREAMFLAGS_AUTOCONVERTPCM,
                                                   BufferDuration,
                                                   0,
                                                   &WaveFormat,
                                                   0) == S_OK)
                {
                    IAudioRenderClient *AudioRenderClient;
                    if(AudioClient->lpVtbl->GetService(AudioClient, IID_IAudioRenderClient, (void **)&AudioRenderClient) == S_OK)
                    {
                        win32_audio *AudioThreadParams = (win32_audio *)Win32AllocateMemory(sizeof(win32_audio));
                        if(AudioThreadParams)
                        {
                            AudioThreadParams->AudioClient = AudioClient;
                            AudioThreadParams->AudioRenderClient = AudioRenderClient;
                            AudioThreadParams->SamplesPerSecond = WaveFormat.nSamplesPerSec;
                            AudioThreadParams->ChannelCount = WaveFormat.nChannels;
                            
                            AudioThreadHandle = CreateThread(0, 0, Win32AudioThread, AudioThreadParams, 0, 0);
                            if(!AudioThreadHandle)
                            {
                                RF_LOG("CreateThread failed to create audio thread with error: %s", Win32GetLastErrorAsString());
                            }
                            else
                            {
                                Result = true;
                            }                            
                        }
                        else
                        {
                            RF_LOG("Failed to allocate buffer for audio thread parameters: %s", Win32GetLastErrorAsString());
                        }                        
                    }
                    else
                    {
                        RF_LOG("GetService failed to retrieve IAudioRenderClient with error: %s", Win32GetLastErrorAsString());
                    }
                }
                else
                {
                    RF_LOG("CreateThread failed to create audio thread with error: %s", Win32GetLastErrorAsString());
                }
            }    
            else
            {
                RF_LOG("Failed to Activate IAudioClient with error: %s", Win32GetLastErrorAsString());
            }
        }
        else
        {
            RF_LOG("Failed to GetDefaultAudioEndpoint with error: %s", Win32GetLastErrorAsString());
        }
    }
    else
    {
        RF_LOG("CoCreateInstance failed to create IMMDeviceEnumerator with error: %s", Win32GetLastErrorAsString());
    }
    
    return(Result);
}

#if DEAR_IMGUI
extern IMGUI_IMPL_API LRESULT ImGui_ImplWin32_WndProcHandler(HWND hWnd, UINT msg, WPARAM wParam, LPARAM lParam);
#endif

static LRESULT CALLBACK
Win32MainWindowCallback(HWND Window, UINT Message, WPARAM WParam, LPARAM LParam)
{
    LRESULT Result = 0;
    
#if DEAR_IMGUI
    if(ShouldUpdateAndDrawIMGUI)
    {
        if (ImGui_ImplWin32_WndProcHandler(Window, Message, WParam, LParam))
            return true;
    }
#endif
    
    switch(Message)
    {
        // TODO(zak): Send a message to the application layer so that it knows its no longer the 
        // active window. This is primarily so people can put the game in a pause state if they want 
        // to when the window is not the main application
        case WM_ACTIVATEAPP:
        {
            GlobalIsWindowActive = !GlobalIsWindowActive;
            ShouldSetMouse = !ShouldSetMouse;
            
            // NOTE(zak): are window isn't active anymore so give the user back control of their mouse            
            static rf_mouse_mode PreviousMouseMode;
            if(!GlobalIsWindowActive)
            {
                PreviousMouseMode = MouseMode;
                Win32SetMouseMode(RF_MOUSE_MODE_APPLICATION);
            }
            
            // NOTE(zak): The window is active so take back whatever control we previously had
            if(GlobalIsWindowActive)
            {
                Win32SetMouseMode(PreviousMouseMode);
            }                        
        } break;
               
        case WM_SIZE:
        {
            WindowWidth = LOWORD(LParam);
            WindowHeight = HIWORD(LParam);
            
            int Width = LOWORD(LParam);
            int Height = HIWORD(LParam);
            
            // NOTE(zak): D3D11CreateDeviceAndSwapChain fires a WM_SIZE command if the swapchain 
            //is fullscreen, which will cause us to crash here as we dont have any of the handles yet.
            if(D3DInitalized)
            {
                D3D11Core.DeviceContext->lpVtbl->OMSetRenderTargets(D3D11Core.DeviceContext, 0, 0, 0);
                SAFE_RELEASE(D3D11Core.RenderTargetView);
                SAFE_RELEASE(D3D11Core.DepthStencilView);

                if(D3D11Core.Swapchain->lpVtbl->ResizeBuffers(D3D11Core.Swapchain, 0, Width, Height, DXGI_FORMAT_UNKNOWN, 0) == S_OK)
                {                    
                    ID3D11Texture2D *BackBuffer;
                    if(D3D11Core.Swapchain->lpVtbl->GetBuffer(D3D11Core.Swapchain, 0, IID_ID3D11Texture2D, (void **)&BackBuffer) == S_OK)
                    {            
                        D3D11_RENDER_TARGET_VIEW_DESC RenderTargetViewDescription = {};
                        RenderTargetViewDescription.Format = DXGI_FORMAT_B8G8R8A8_UNORM_SRGB;
                        RenderTargetViewDescription.ViewDimension = D3D11_RTV_DIMENSION_TEXTURE2D;
                        RenderTargetViewDescription.Texture2D.MipSlice = 0;
                        
                        if(D3D11Core.Device->lpVtbl->CreateRenderTargetView(D3D11Core.Device, (ID3D11Resource * )BackBuffer, &RenderTargetViewDescription, &D3D11Core.RenderTargetView) == S_OK)
                        {
                            D3D11Core.StableRenderTargetView = &D3D11Core.RenderTargetView;
                            
                            SAFE_RELEASE(BackBuffer);
                            
                            D3D11_TEXTURE2D_DESC DepthStencilBufferDesc = {0};
                            DepthStencilBufferDesc.ArraySize = 1;
                            DepthStencilBufferDesc.BindFlags = D3D11_BIND_DEPTH_STENCIL;
                            DepthStencilBufferDesc.CPUAccessFlags = 0;
                            DepthStencilBufferDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
                            DepthStencilBufferDesc.Width = Width;
                            DepthStencilBufferDesc.Height = Height;
                            DepthStencilBufferDesc.MipLevels = 1;
                            DepthStencilBufferDesc.SampleDesc.Count = 1;
                            DepthStencilBufferDesc.SampleDesc.Quality = 0;
                            DepthStencilBufferDesc.Usage = D3D11_USAGE_DEFAULT;
                            
                            ID3D11Texture2D *DepthStencilBuffer;
                            if(D3D11Core.Device->lpVtbl->CreateTexture2D(D3D11Core.Device, &DepthStencilBufferDesc, 0, &DepthStencilBuffer) == S_OK)
                            {
                                if(D3D11Core.Device->lpVtbl->CreateDepthStencilView(D3D11Core.Device, (ID3D11Resource * )DepthStencilBuffer, 0, &D3D11Core.DepthStencilView) == S_OK)
                                {
                                    D3D11Core.StableDepthStencilView = &D3D11Core.DepthStencilView;
                                    SAFE_RELEASE(DepthStencilBuffer);
                                    
                                    D3D11_VIEWPORT Viewport = {0};
                                    Viewport.Width = Width;
                                    Viewport.Height = Height;
                                    Viewport.MaxDepth = 1.0f;
                                    
                                    D3D11Core.DeviceContext->lpVtbl->RSSetViewports(D3D11Core.DeviceContext, 1, &Viewport);
                                }
                            }
                        }
                    }
                }                
            }
            
            rf_platform_event Event = {};
            Event.Type = RF_EVENT_TYPE_RESIZE;
            Event.ResizeInfo.Width = Width;
            Event.ResizeInfo.Height = Height;
            Events[CurrentEvent++] = Event;            
        } break;
                
        case WM_INPUT:
        {       
            UINT Size = sizeof(RAWINPUT);
            // 
            // NOTE(zak): So i tried to get away with not calling GetRawInputData twice, but 
            // it seems like RIM_TYPEKEYBOARD does not get broadcasted if we do that 
            // note sure why but for not we will leave it like this
            if(GetRawInputData((HRAWINPUT)LParam, RID_INPUT, 0, &Size, sizeof(RAWINPUTHEADER)) == 0)
            {
                // NOTE(zak): This buffer's max size that if seen is the size of RAWINPUT
                // so were just gonna constantly allocate a buffer of that size on the stack
                // to prevent us from having to do a heap allocation every single frame
                BYTE *Buffer = (BYTE *)_alloca(Size);
                if(Buffer)
                {
                    if(GetRawInputData((HRAWINPUT)LParam, RID_INPUT, Buffer, &Size, sizeof(RAWINPUTHEADER)) == Size)
                    {
                        RAWINPUT *RawInput = (RAWINPUT *)Buffer;
                        if(RawInput)
                        {
                            if(RawInput->header.dwType == RIM_TYPEMOUSE)
                            {
                                USHORT ButtonFlags = RawInput->data.mouse.usButtonFlags;
                                
                                // If any mouse button was down lets create a event
                                if(ButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN || ButtonFlags & RI_MOUSE_LEFT_BUTTON_UP || ButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN || ButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP)
                                {
                                    rf_platform_event Event = {};
                                    Event.Type = RF_EVENT_TYPE_MOUSE;

                                    Event.MouseInfo.Type = MOUSE_EVENT_INPUT;                                    
                                    
                                    if(ButtonFlags & RI_MOUSE_LEFT_BUTTON_DOWN)
                                    {
                                        Event.MouseInfo.MouseButton = RF_PLATFORM_MOUSE_BUTTON_LEFT;
                                        Event.MouseInfo.IsDown = 1;
                                    }
                                    else if(ButtonFlags & RI_MOUSE_LEFT_BUTTON_UP)
                                    {
                                        Event.MouseInfo.MouseButton = RF_PLATFORM_MOUSE_BUTTON_LEFT;
                                        Event.MouseInfo.IsDown = 0;
                                    }
                                    else if(ButtonFlags & RI_MOUSE_MIDDLE_BUTTON_DOWN)
                                    {
                                        Event.MouseInfo.MouseButton = RF_PLATFORM_MOUSE_BUTTON_MIDDLE;
                                        Event.MouseInfo.IsDown = 1;
                                    }
                                    else if(ButtonFlags & RI_MOUSE_MIDDLE_BUTTON_UP)
                                    {
                                        Event.MouseInfo.MouseButton = RF_PLATFORM_MOUSE_BUTTON_MIDDLE;
                                        Event.MouseInfo.IsDown = 0;
                                    }
                                    else if(ButtonFlags & RI_MOUSE_RIGHT_BUTTON_DOWN)
                                    {
                                        Event.MouseInfo.MouseButton = RF_PLATFORM_MOUSE_BUTTON_RIGHT;
                                        Event.MouseInfo.IsDown = 1;
                                    }
                                    else if (ButtonFlags & RI_MOUSE_RIGHT_BUTTON_UP)
                                    {
                                        Event.MouseInfo.MouseButton = RF_PLATFORM_MOUSE_BUTTON_RIGHT;
                                        Event.MouseInfo.IsDown = 0;
                                    }
                                    
                                    Events[CurrentEvent++] = Event;                                  
                                }                               
                            }                            
                            else if(RawInput->header.dwType == RIM_TYPEKEYBOARD)
                            {
                                USHORT KeyCode = RawInput->data.keyboard.VKey;
                                USHORT ScanCode = RawInput->data.keyboard.MakeCode;
                                USHORT Flags = RawInput->data.keyboard.Flags;
                                
                                // NOTE(zak): Some key using escaped sequences will send fake keys with 
                                // a KeyCode of 255, so lets just ignore all of those.
                                if(KeyCode != 255)
                                {
                                    // NOTE(zak): https://blog.molecular-matters.com/2011/09/05/properly-handling-keyboard-input/
                                    // Some special case VKCode that RAWINPUT doesn't really handle "correctly"
                                    // so lets handle them here ourselves
                                    if(KeyCode == VK_SHIFT)
                                    {
                                        KeyCode = (USHORT)MapVirtualKey(ScanCode, MAPVK_VSC_TO_VK_EX);                                    
                                    }
                                    else if(KeyCode == VK_NUMLOCK)
                                    {
                                        ScanCode = (USHORT)(MapVirtualKey(KeyCode, MAPVK_VK_TO_VSC) | 0x100);
                                    }
                                    
                                    
                                    rf_b32 bIsDown = false;
                                    
                                    if(Flags == RI_KEY_MAKE)
                                    {
                                        bIsDown = true;
                                    }
                                    
                                    rf_platform_event Event = {};
                                    Event.Type = RF_EVENT_TYPE_KEYBOARD;
                                    
                                    Event.KeyboardInfo.KeyCode = KeyCode;                                                                        
                                    Event.KeyboardInfo.IsDown =  bIsDown;
                                    
									Events[CurrentEvent++] = Event;
                                }
                            }
                            else if(RawInput->header.dwType == RIM_TYPEHID)
                            {       
                                UINT DevInfoBufferSize = 0;
                                if(GetRawInputDeviceInfoA(RawInput->header.hDevice,
                                                          RIDI_DEVICEINFO,
                                                          0,
                                                          &DevInfoBufferSize) == 0)
                                {
                                    RID_DEVICE_INFO *DevInfo = (RID_DEVICE_INFO *)_alloca(DevInfoBufferSize);
                                    
                                    if(DevInfo)
                                    {
                                        if(GetRawInputDeviceInfoA(RawInput->header.hDevice,
                                                                  RIDI_DEVICEINFO,
                                                                  DevInfo,
                                                                  &DevInfoBufferSize) >= 0 )
                                        {
                                            // 0x054C - Sony Corp vendor id
                                            // 0x05C4/0x09cc - PS4 controller product id theres two generations of controllers
                                            // 0x0100 - PS4 controller version 1
                                            
                                            if(DevInfo->hid.dwVendorId == 0x054C &&
                                               ((DevInfo->hid.dwProductId == 0x09cc) ||
                                                (DevInfo->hid.dwProductId == 0x05C4)) &&
                                               DevInfo->hid.dwVersionNumber == 0x0100)
                                            {
                                                // NOTE(zak): This is a PS4 controller. We support 
                                                // this so not lets get its input
                                                
                                                UINT PreparsedDataBufferSize = 0;
                                                
                                                if(GetRawInputDeviceInfoA(RawInput->header.hDevice,
                                                                          RIDI_PREPARSEDDATA,
                                                                          0,
                                                                          &PreparsedDataBufferSize) == 0)
                                                {
                                                    PHIDP_PREPARSED_DATA PreparsedData = (PHIDP_PREPARSED_DATA)_alloca(PreparsedDataBufferSize);
                                                    if(PreparsedData)
                                                    {
                                                        if(GetRawInputDeviceInfoA(RawInput->header.hDevice,
                                                                                  RIDI_PREPARSEDDATA,
                                                                                  PreparsedData,
                                                                                  &PreparsedDataBufferSize) >= 0)
                                                        {
                                                            HIDP_CAPS Caps;
                                                            if(HidP_GetCaps(PreparsedData, &Caps) == HIDP_STATUS_SUCCESS)
                                                            {
                                                                PHIDP_BUTTON_CAPS ButtonCaps = (PHIDP_BUTTON_CAPS)_alloca(sizeof(HIDP_BUTTON_CAPS) * Caps.NumberInputButtonCaps);
                                                                if(ButtonCaps)
                                                                {
                                                                    size_t CapsLength = Caps.NumberInputButtonCaps;
                                                                    if(HidP_GetButtonCaps(HidP_Input, ButtonCaps, (PUSHORT)&CapsLength, PreparsedData) == HIDP_STATUS_SUCCESS)
                                                                    {
                                                                        size_t NumButtons = ButtonCaps->Range.UsageMax - ButtonCaps->Range.UsageMin + 1;
                                                                        PHIDP_VALUE_CAPS ValueCaps = (PHIDP_VALUE_CAPS)_alloca(sizeof(HIDP_VALUE_CAPS) * Caps.NumberInputValueCaps);
                                                                        if(ValueCaps)
                                                                        {
                                                                            if(HidP_GetValueCaps(HidP_Input, ValueCaps, &Caps.NumberInputValueCaps, PreparsedData) == HIDP_STATUS_SUCCESS)
                                                                            {
                                                                                ULONG UsageLength = 0;
                                                                                USAGE Usage[128];
                                                                                ZeroMemory(Usage, sizeof(Usage));
                                                                                if(HidP_GetUsages(HidP_Input, ButtonCaps->UsagePage, 0, 0, &UsageLength, PreparsedData, (PCHAR)RawInput->data.hid.bRawData, RawInput->data.hid.dwSizeHid) == HIDP_STATUS_SUCCESS)
                                                                                {
                                                                                    BOOL ButtonStates[128];
                                                                                    ZeroMemory(ButtonStates, sizeof(ButtonStates));
                                                                                    
                                                                                    if(UsageLength > 0)
                                                                                    {
                                                                                        __debugbreak();
                                                                                    }
                                                                                    
                                                                                    //for(int IIndex = 0; IIndex < NumButtons; IIndex++)
                                                                                    //{
                                                                                    //    int RealIndex = Usage[IIndex] - ButtonCaps->Range.UsageMin;
                                                                                    //    ButtonStates[RealIndex] = 1;
                                                                                    //}
                                                                                    
                                                                                    //for(int Index = 0; Index < NumButtons; ++Index)
                                                                                    //{
                                                                                    //    RF_LOG("%d: %d", Index, ButtonStates[Index]);
                                                                                    //}
                                                                                    
                                                                                }
                                                                            }
                                                                        }                                                                            
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }                                                    
                                                }                                                
                                            }
                                        }                                          
                                    }
                                }
                            }
                        }
                    }                                                            
                }                
            }

        } break;
        
        case WM_MOUSEMOVE:
        {
                rf_platform_event Event = {};
                Event.Type = RF_EVENT_TYPE_MOUSE;
                Event.MouseInfo.Type = MOUSE_EVENT_MOVE;
                Event.MouseInfo.MouseX = ((int)(short)LOWORD(LParam));
                Event.MouseInfo.MouseY = ((int)(short)HIWORD(LParam));
                Events[CurrentEvent++] = Event;
        } break;        
       
        case WM_DEVICECHANGE:
        {
            UpdateXInputControllerList = true;
        } break;
    
        case WM_DESTROY:
        {
            GlobalRunning = 0;
        } break;
        
        case WM_CLOSE:
        {
            GlobalRunning = 0;
        } break;
        
        case WM_TIMER:
        {
            SwitchToFiber(Win32MainFiber);
        } break;
        
        default:
        {
            Result = DefWindowProcA(Window, Message, WParam, LParam);
        } break;
    }
    
    return(Result);
}

static win32_game
Win32LoadGame(char *SourceFilePath, char *TempFilePath, char *LockFilePath)
{
    win32_game Result = {0};
    
#if DISABLE_HOT_RELOADING
    // NOTE(zak): These parameters aren't used in this code path and will generate warnings
    UNREFERENCED_PARAMETER(SourceFilePath);
    UNREFERENCED_PARAMETER(TempFilePath);
    UNREFERENCED_PARAMETER(LockFilePath);

    // HACK HACK HACK
    int NumArgs = 0;
    CommandLineToArgvW(GetCommandLineW(), &NumArgs);

    LPSTR CommandLine = GetCommandLineA();
    AppDescription = PlatformLayer_EntryPoint(NumArgs, &CommandLine);
    Result.Loaded = true;
#else
    // NOTE(zak): If the Lock File doesn't exist we are not building and we can try for a hot reload
    WIN32_FILE_ATTRIBUTE_DATA Ignored;
    if(!GetFileAttributesExA(LockFilePath, GetFileExInfoStandard, &Ignored))
    {
        Result.LastWriteTime = Win32GetLastWriteTime(SourceFilePath);
        
        CopyFileA(SourceFilePath, TempFilePath, 0);
        
        Result.GameDLL = LoadLibraryA(TempFilePath);
        if(Result.GameDLL)
        {
            Result.Entrypoint = (rf_platform_layer_entrypoint *)GetProcAddress(Result.GameDLL, "PlatformLayer_EntryPoint");
            if(Result.Entrypoint)
            {
				// HACK HACK HACK
				int NumArgs = 0;
				CommandLineToArgvW(GetCommandLineW(), &NumArgs);

				LPSTR CommandLine = GetCommandLineA();
                AppDescription = Result.Entrypoint(NumArgs, &CommandLine);
                
                if(AppDescription.Init && AppDescription.Shutdown && AppDescription.Update && AppDescription.Render)
                {
                    Result.Loaded = true; 
                }
            }
            else
            {
                RF_LOG("Failed to get Applications Entrypoint");
            }            
        }
        else
        {
            RF_LOG("Failed to load games DLL with error: %s", Win32GetLastErrorAsString());
        }
    }    
#endif
    
    return(Result);
}

static void 
Win32UnloadGame(win32_game *Game)
{
    if(Game->GameDLL)    
    {
        FreeLibrary(Game->GameDLL);
        Game->GameDLL = 0;
    }
    
    AppDescription.Init = 0;
    AppDescription.Shutdown = 0;
    AppDescription.Update = 0;
    AppDescription.Render = 0;
    AppDescription.AudioCallback = 0;
    Game->Loaded = 0;
}

static void
Win32GetEXEFileName(win32_state *State)
{
    DWORD SizeOfFilename = GetModuleFileNameA(0, State->EXEFileName, sizeof(State->EXEFileName));   
    State->OnePastLastEXEFileNameSlash = State->EXEFileName;
    
    for(char* Scan = State->EXEFileName; *Scan; ++Scan)
    {
        if(*Scan == '\\')
        {
            State->OnePastLastEXEFileNameSlash = Scan + 1;
        }
    }       
}

static void
CStringCat(size_t SourceACount, char *SourceA,
           size_t SourceBCount, char *SourceB,
           size_t DestCount, char *Dest)
{
    UNREFERENCED_PARAMETER(DestCount);
    
    for(size_t Index = 0;
        Index < SourceACount;
        ++Index)
    {
        *Dest++ = *SourceA++;
    }
    
    for(size_t Index = 0;
        Index < SourceBCount;
        ++Index)
    {
        *Dest++ = *SourceB++;
    }
    
    *Dest++ = 0;
}

static void
Win32BuildEXEPathFileName(win32_state *State, char *FileName,
                          int DestCount, char *Dest)
{
    CStringCat(State->OnePastLastEXEFileNameSlash - State->EXEFileName, State->EXEFileName,
               CStringLength(FileName), FileName,
               DestCount, Dest);
}

static rf_b32
Win32LoadXInput()
{
    rf_b32 Result = true;
    
    HMODULE XInputLib = LoadLibraryA("xinput1_4.dll");
    if(!XInputLib)
    {
        RF_LOG("XInput1_4.dll was not found!");
               
        XInputLib = LoadLibraryA("xinput9_1_0.dll");
        if(!XInputLib)
        {
            RF_LOG("XInput9_1_0.dll was not found!");    
            
            
            XInputLib = LoadLibraryA("xinput1_3.dll");
            if(!XInputLib)
            {
                RF_LOG("XInput1_3.dll was not found!");    
                RF_LOG("XInput could not be loaded!");    
                
                Result = false;
            }
        }        
    }
    
    if(XInputLib)
    {
        XInputGetState_ = (xinput_get_state *)GetProcAddress(XInputLib, "XInputGetState");
        if(!XInputGetState_)
        {
            RF_LOG("Could not load function pointer for XInputGetState!");
        }                
        
        XInputSetState_ = (xinput_set_state *)GetProcAddress(XInputLib, "XInputSetState");
        if(!XInputSetState_)
        {
            RF_LOG("Could not load function pointer for XInputSetState!");            
        }        
    }
    
    return(Result);
}

static rf_platform_gamepad_button
Win32XInputButtonIDToRfGamepadButton(int ButtonID)
{
    rf_platform_gamepad_button Result;
    
    switch(ButtonID)
    {
        case XINPUT_GAMEPAD_DPAD_UP:
        {
            Result = RF_PLATFORM_GAMEPAD_DPAD_UP;
        } break;
        
        case XINPUT_GAMEPAD_DPAD_DOWN:
        {
            Result = RF_PLATFORM_GAMEPAD_DPAD_DOWN;
        } break;        
        
        case XINPUT_GAMEPAD_DPAD_LEFT:
        {
            Result = RF_PLATFORM_GAMEPAD_DPAD_LEFT;
        } break;        
        
        case XINPUT_GAMEPAD_DPAD_RIGHT:
        {
            Result = RF_PLATFORM_GAMEPAD_DPAD_RIGHT;
        } break;
        
        case XINPUT_GAMEPAD_START:        
        {
            Result = RF_PLATFORM_GAMEPAD_START;
        } break;
        
        case XINPUT_GAMEPAD_BACK:        
        {
            Result = RF_PLATFORM_GAMEPAD_BACK;
        } break;
        
        case XINPUT_GAMEPAD_LEFT_THUMB:
        {
            Result = RF_PLATFORM_GAMEPAD_LEFT_THUMB;
        } break;
        
        case XINPUT_GAMEPAD_RIGHT_THUMB:
        {
            Result = RF_PLATFORM_GAMEPAD_RIGHT_THUMB;
        } break;
        
        case XINPUT_GAMEPAD_LEFT_SHOULDER:
        {
            Result = RF_PLATFORM_GAMEPAD_LEFT_SHOULDER;
        } break;
        
        case XINPUT_GAMEPAD_RIGHT_SHOULDER:
        {
            Result = RF_PLATFORM_GAMEPAD_RIGHT_SHOULDER;
        } break;
        
        case XINPUT_GAMEPAD_A:
        {
            Result = RF_PLATFORM_GAMEPAD_A;
        } break;
        
        case XINPUT_GAMEPAD_B:
        {
            Result = RF_PLATFORM_GAMEPAD_B;
        } break;
        
        case XINPUT_GAMEPAD_X:
        {
            Result = RF_PLATFORM_GAMEPAD_X;
        } break;
        
        case XINPUT_GAMEPAD_Y:
        {
            Result = RF_PLATFORM_GAMEPAD_Y;
        } break;                    
    }
    
    return(Result);
}

static void
Win32XInputProcessButton(XINPUT_STATE *State, int ButtonID)
{
    bool ShouldSendMessage = false;
    
    rf_b32 WasDown =(PreviousState.Gamepad.wButtons & ButtonID) && !(State->Gamepad.wButtons & ButtonID);
    rf_b32 IsDown = (PreviousState.Gamepad.wButtons & ButtonID) && (State->Gamepad.wButtons & ButtonID);
    rf_b32 WasUp = !(PreviousState.Gamepad.wButtons & ButtonID) && (State->Gamepad.wButtons & ButtonID);
                                                                     
    if(WasDown || IsDown || WasUp)
    {
        ShouldSendMessage = true;
    }
    
    if(ShouldSendMessage)
    {
        rf_b32 IsDown = false;
        if(State->Gamepad.wButtons & ButtonID)
        {
            IsDown = true;
        }
        
        rf_platform_event Event = {};
        Event.Type = RF_EVENT_TYPE_GAMEPAD;
        Event.GamepadInfo.Type = RF_PLATFORM_GAMEPAD_EVENT_TYPE_BUTTON;
        Event.GamepadInfo.Button = Win32XInputButtonIDToRfGamepadButton(ButtonID);
        Event.GamepadInfo.IsDown = IsDown;
        
        Events[CurrentEvent++] = Event;
    }
}

static void CALLBACK
Win32MessageFiberProc(void *Data)
{
    SetTimer((HWND)Data, 1, 1, 0);
    for (;;) 
    {
        MSG message;
        while (PeekMessage(&message, 0, 0, 0, PM_REMOVE)) 
        {
            TranslateMessage(&message);
            DispatchMessage(&message);
        }
        
        SwitchToFiber(Win32MainFiber);
    }    
}


typedef struct rf_platform_work_queue_entry
{
    rf_platform_work_queue_callback *Callback;
    void *Data;
} platform_work_queue_entry;

typedef struct rf_platform_work_queue
{    
    volatile rf_u32 CompletionGoal;
    volatile rf_u32 CompletionCount;
    volatile rf_u32 NextEntryToWrite;
    volatile rf_u32 NextEntryToRead;
    HANDLE SemaphoreHandle;
    
    platform_work_queue_entry * Entries;
} platform_work_queue;

typedef struct win32_thread_info
{
    rf_u32 LogicalThreadIndex;
    platform_work_queue *HighPriorityQueue;
    platform_work_queue *LowPriorityQueue;
} win32_thread_info;

static void
Win32AddEntry(platform_work_queue *Queue, rf_platform_work_queue_callback *Callback, void *Data)
{
    rf_u32 NewNextEntryToWrite = (Queue->NextEntryToWrite + 1) % FUCKYOUTHREADSIZE;
    RfPlatform_Assert(NewNextEntryToWrite != Queue->NextEntryToRead);
    
    platform_work_queue_entry *Entry = Queue->Entries + Queue->NextEntryToWrite;
    Entry->Data = Data;
    Entry->Callback = Callback;
    ++Queue->CompletionGoal;
    
    _WriteBarrier();
    _mm_sfence();    
    Queue->NextEntryToWrite = NewNextEntryToWrite;    
    ReleaseSemaphore(Queue->SemaphoreHandle, 1, 0);
}

static rf_b32
Win32DoNextWorkQueueEntry(platform_work_queue *Queue)
{
    rf_b32 WeShouldSleep = false;
    
    rf_u32 OriginalNextEntry = Queue->NextEntryToRead;
    rf_u32 NewNextEntryToRead = (OriginalNextEntry + 1) % FUCKYOUTHREADSIZE;

    if(Queue->NextEntryToRead != Queue->NextEntryToWrite)
    {                         
        rf_u32 Index = InterlockedCompareExchange((LONG volatile *)&Queue->NextEntryToRead, NewNextEntryToRead, OriginalNextEntry);
        if(Index == OriginalNextEntry)
        {
            platform_work_queue_entry Entry = Queue->Entries[Index];
            Entry.Callback(Queue, Entry.Data);
            InterlockedIncrement((LONG volatile *)&Queue->CompletionCount);                 
        }
    }
    else
    {
        WeShouldSleep = true;
    }
    
    return(WeShouldSleep);
}

static void 
Win32CompleteAllWork(platform_work_queue *Queue)
{   
    while(Queue->CompletionGoal != Queue->CompletionCount)
    {
        Win32DoNextWorkQueueEntry(Queue);        
    }    
    
    Queue->CompletionGoal = 0;
    Queue->CompletionCount = 0;
}


DWORD WINAPI
Win32WorkerThreadProc(LPVOID Parameter)
{
    win32_thread_info *ThreadInfo = (win32_thread_info *)Parameter;
    
    for(;;)
    {
        if(Win32DoNextWorkQueueEntry(ThreadInfo->HighPriorityQueue))
        {
            if(Win32DoNextWorkQueueEntry(ThreadInfo->LowPriorityQueue))
            {
                WaitForSingleObjectEx(ThreadInfo->HighPriorityQueue->SemaphoreHandle, INFINITE, FALSE);
            }
        }        
    }
    
    
    return(0);
}

static void
Win32AddJobToHighPriorityQueue(rf_platform_work_queue_callback *Callback, void *Data)
{
    Win32AddEntry(RfPlatform.HighPriorityQueue, Callback, Data);
}

static void
Win32AddJobToLowPriorityQueue(rf_platform_work_queue_callback *Callback, void *Data)
{
    Win32AddEntry(RfPlatform.LowPriorityQueue, Callback, Data);
}

inline bool 
AreStringsEqual(LPWSTR StringOne, LPWSTR StringTwo)
{
    while(*StringOne)
    {
        if(*StringOne != *StringTwo)
        {
            return false;            
        }
        
        *StringOne++;
        *StringTwo++;
    }
    
    return true;
}

int WINAPI
WinMain(HINSTANCE Instance,
        HINSTANCE PrevInstance,
        LPSTR CommandLine,
        int ShowCode)
{
    UNREFERENCED_PARAMETER(PrevInstance);
    UNREFERENCED_PARAMETER(CommandLine);
    UNREFERENCED_PARAMETER(ShowCode);
    
#if DEBUG
    AllocConsole();
    freopen("CONOUT$", "w+", stdout);
    printf("Hello, World!\n");
#endif
    
#if DEAR_IMGUI
    ImGui_ImplWin32_EnableDpiAwareness();   
#endif
    
    
    rf_b32 IsXInputOn = Win32LoadXInput();
    
	RfPlatform.D3DGetDevice = Win32GetD3DDevice;
	RfPlatform.D3DGetDeviceContext = Win32GetD3DDeviceContext;
	RfPlatform.D3DGetRenderTargetView = Win32GetD3DRenderTargetView;
	RfPlatform.D3DGetFeatureLevel = Win32GetD3DFeatureLevel;
	RfPlatform.D3DGetDepthStencilView = Win32GetD3DDepthStencilView;
	RfPlatform.OpenFile = Win32OpenFile;
	RfPlatform.GetFileSize = Win32GetFileSize;
	RfPlatform.ReadFile = Win32ReadFile;
	RfPlatform.WriteFile = Win32WriteFile;
    RfPlatform.Log = Win32Log;
    RfPlatform.AllocateMemory = Win32AllocateMemory;
    RfPlatform.FreeMemory = Win32FreeMemory;
    RfPlatform.AddJobToQueue = Win32AddEntry;
    RfPlatform.CompleteAllJobsInQueue = Win32CompleteAllWork;
    RfPlatform.AddJobToHighPriorityQueue = Win32AddJobToHighPriorityQueue;
    RfPlatform.AddJobToLowPriorityQueue = Win32AddJobToLowPriorityQueue;
    RfPlatform.GetWindowWidth = Win32GetWindowWidth;
    RfPlatform.GetWindowHeight = Win32GetWindowHeight;
    RfPlatform.SetMouseMode = Win32SetMouseMode;
    RfPlatform.LoadImage = Win32LoadImage;
    RfPlatform.SetShouldDrawIMGUI = Win32SetShouldDrawIMGUI;
    
    LARGE_INTEGER PerfCountFrequencyResult;    
    QueryPerformanceFrequency(&PerfCountFrequencyResult);
    GlobalPerfCountFrequency = (rf_f64)PerfCountFrequencyResult.QuadPart;    
    
    win32_state State = {0};
    Win32GetEXEFileName(&State);    
    
    char SourceGameCodeDLLFullPath[MAX_PATH];
    Win32BuildEXEPathFileName(&State, "game.dll",
                              sizeof(SourceGameCodeDLLFullPath), SourceGameCodeDLLFullPath);
    
    char TempGameCodeDLLFullPath[MAX_PATH];
    Win32BuildEXEPathFileName(&State, "game_temp.dll",
                              sizeof(TempGameCodeDLLFullPath), TempGameCodeDLLFullPath);    
    
    
    char GameCodeLockFullPath[MAX_PATH];
    Win32BuildEXEPathFileName(&State, "lock.tmp",
                              sizeof(GameCodeLockFullPath), GameCodeLockFullPath);
    
    win32_game Game = Win32LoadGame(SourceGameCodeDLLFullPath, TempGameCodeDLLFullPath, GameCodeLockFullPath);
    if(Game.Loaded)
    {                
        if(AppDescription.NeedsNetworking)
        {
            // NOTE(zak): I cant see any reason to use winsock 2 over winsock 1 so 
            // for now we will use one. Later on change this if we find any issues with it
            WSADATA WSAData = {0};
            if(WSAStartup(MAKEWORD(1, 1), &WSAData) != 0)
            {
                RF_LOG("WSAStartup failed with error: %s", Win32GetLastErrorAsString());
            }
        }
        
        // NOTE(zak): Make all extra cores worker threads
        
        platform_work_queue HighPriorityQueue = {0};
        HighPriorityQueue.Entries = (platform_work_queue_entry *)Win32AllocateMemory(sizeof(platform_work_queue_entry)*FUCKYOUTHREADSIZE);
        platform_work_queue LowPriorityQueue = {0};
        LowPriorityQueue.Entries = (platform_work_queue_entry *)Win32AllocateMemory(sizeof(platform_work_queue_entry)*FUCKYOUTHREADSIZE);
        RfPlatform.HighPriorityQueue = &HighPriorityQueue;
        RfPlatform.LowPriorityQueue = &LowPriorityQueue;
            
        SYSTEM_INFO SystemInfo = {0};
        GetSystemInfo(&SystemInfo);                
        const int NumWorkerThreads = SystemInfo.dwNumberOfProcessors - 2; // -2 for main thread and audio thread.
        
        win32_thread_info ThreadInfo[NumWorkerThreads];        
        
        HighPriorityQueue.SemaphoreHandle = CreateSemaphoreEx(0, 0, NumWorkerThreads, 0, 0, SEMAPHORE_ALL_ACCESS);
        LowPriorityQueue.SemaphoreHandle = HighPriorityQueue.SemaphoreHandle;
        
        for(int Index = 0; Index < ArrayCount(ThreadInfo); ++Index)
        {
            win32_thread_info *Info = ThreadInfo + Index;
            Info->LogicalThreadIndex = Index;
            Info->HighPriorityQueue = &HighPriorityQueue;
            Info->LowPriorityQueue = &LowPriorityQueue;
            CreateThread(0, 0, Win32WorkerThreadProc, Info, 0, 0);
        }
        
        WNDCLASSA WindowClass = {0};
        WindowClass.style = CS_HREDRAW|CS_VREDRAW|CS_OWNDC;
        WindowClass.lpfnWndProc = Win32MainWindowCallback;
        WindowClass.hInstance = Instance;
        WindowClass.hCursor = LoadCursor(NULL, IDC_ARROW);
        WindowClass.hIcon = LoadIcon(NULL, IDI_WINLOGO);
        WindowClass.lpszClassName = "RfPlatformWindowClass";
        
        if(RegisterClassA(&WindowClass))
        {
            RECT WindowRect = {0};
            WindowRect.right = AppDescription.WindowWidth;
            WindowRect.bottom = AppDescription.WindowHeight;
            AdjustWindowRect(&WindowRect, WS_OVERLAPPEDWINDOW, FALSE);
            
            rf_u32 NewWindowWidth = (WindowRect.right - WindowRect.left);
            rf_u32 NewWindowHeight = (WindowRect.bottom - WindowRect.top);
            rf_u32 WindowCenteredX = ((GetSystemMetrics(SM_CXSCREEN) - NewWindowWidth) / 2);
            rf_u32 WindowCenteredY = ((GetSystemMetrics(SM_CYSCREEN) - NewWindowHeight) / 2);
            
            HWND Window = 
                CreateWindowExA(0,
                                WindowClass.lpszClassName,
                                AppDescription.WindowTitle,
                                WS_OVERLAPPEDWINDOW,
                                WindowCenteredX,
                                WindowCenteredY, 
                                NewWindowWidth,
                                NewWindowHeight,
                                0, 0, Instance, 0);            
            
            if(Window)
            {                                                                
                Win32MainFiber = ConvertThreadToFiber(0);
                Win32MessageFiber = CreateFiber(0, (PFIBER_START_ROUTINE)Win32MessageFiberProc, Window);                
                
                RAWINPUTDEVICE RawInputDevices[3];
                RawInputDevices[0].usUsagePage = 0x01; 
                RawInputDevices[0].usUsage = 0x02; 
                RawInputDevices[0].dwFlags = 0;
                RawInputDevices[0].hwndTarget = Window;
                
                RawInputDevices[1].usUsagePage = 0x01; 
                RawInputDevices[1].usUsage = 0x06; 
                RawInputDevices[1].dwFlags = 0;
                RawInputDevices[1].hwndTarget = Window;
                               
                RawInputDevices[2].usUsagePage = 0x01; 
                RawInputDevices[2].usUsage = 0x05; 
                RawInputDevices[2].dwFlags = 0;
                RawInputDevices[2].hwndTarget = Window;
                                
                if(RegisterRawInputDevices(RawInputDevices, ArrayCount(RawInputDevices), sizeof(RAWINPUTDEVICE)))
                {
                    UpdateXInputControllerList = true;
                                        
                    D3DInitalized = Win32InitD3D11(Window);
                    if(D3DInitalized)
                    {
                        // NOTE(zak): CoInitializeEx fails on some older machines (for whatever reason) so if we cant do that lets intialize com the old way                        
                        if(CoInitializeEx(0, COINIT_MULTITHREADED) != S_OK)
                        {                                
                            RF_LOG("CoInitializeEx failed with %s, trying CoInitialize.", Win32GetLastErrorAsString());
                            if(CoInitialize(0) != S_OK)
                            {
                                RF_LOG("CoInitialize failed with %s.", Win32GetLastErrorAsString());
                            }

                        }
                        
                        if(AppDescription.NeedsAudio)
                        {
                            if(!Win32InitWASAPI())
                            {
                                RF_LOG("Win32InitWASAPI failed with %s.", Win32GetLastErrorAsString());
                            }
                        }
                        
                        // TODO(zak): ShowWindow sends us a WM_SIZE message that we 
                        // dont really need since the window wasn't really resized but we resize 
                        // the d3d11 backbuffer anyways like normal. Handle this at some point if 
                        // we want to 
                        
                        if(ShowWindow(Window, SW_SHOWNORMAL) == 0)
                        {              
#if DEAR_IMGUI
                            IMGUI_CHECKVERSION();
                            ImGui::CreateContext();
                            ImGuiIO& IO = ImGui::GetIO(); (void)IO;
                            IO.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard; 
                            IO.ConfigFlags |= ImGuiConfigFlags_DockingEnable;
                            IO.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
                            IO.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleFonts;
                            IO.ConfigFlags |= ImGuiConfigFlags_DpiEnableScaleViewports;
                            ImGui::StyleColorsDark();
                            
                            ImGuiStyle& Style = ImGui::GetStyle();
                            if (IO.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
                            {
                                Style.WindowRounding = 0.0f;
                                Style.Colors[ImGuiCol_WindowBg].w = 1.0f;
                            }
                            
                            // Setup Platform/Renderer bindings
                            ImGui_ImplWin32_Init(Window); 
                            ImGui_ImplDX11_Init(D3D11Core.Device, D3D11Core.DeviceContext);
#endif
                            
                            
                            AppDescription.Init(&RfPlatform);
                            
                            // TODO(zak): We are doing this twice, once for the d3d11 initialization
                            // and once for this timer to keep track of the dt. Some day do this once 
                            // and pass this into the win32initd3d11 function
                            int MonitorRefreshHz = 60;
                            int Win32RefreshRate = GetDeviceCaps(GetDC(Window), VREFRESH);
                            if(Win32RefreshRate > 1)
                            {
                                MonitorRefreshHz = Win32RefreshRate;
                            }
                            
                            rf_u32 ExpectedFramesPerUpdate = 1;
                            rf_f32 TargetSecondsPerFrame = (rf_f32)ExpectedFramesPerUpdate / (rf_f32)MonitorRefreshHz;
                            
                            LARGE_INTEGER LastCounter = Win32GetWallClock();

                            GlobalRunning = 1;                           
                            ShouldSetMouse = 1;
                            
                            while(GlobalRunning)
                            {   
                                ShouldUpdateAndDrawIMGUI = BetweenShouldUpdateAndDrawIMGUI;

                                float DeltaTime = TargetSecondsPerFrame;
                                
#if DEBUG && !DISABLE_HOT_RELOADING
                                FILETIME WriteTime = Win32GetLastWriteTime(SourceGameCodeDLLFullPath);
                                if(CompareFileTime(&WriteTime, &Game.LastWriteTime))
                                {
                                    // NOTE(zak): This loop can sometimes run a few times for a new DLL 
                                    // being produced as the WFS kinda sucks. So basically we will shutdown and unload the 
                                    // DLL and try to load the game dll every frame until we can. The notification for 
                                    // us being able to load the game dll is the locking file not existing anymore.
                                    // we delete this right after we finish the file DLL build but sometimes it takes
                                    // awhile                                
                                    
                                    if(AppDescription.Shutdown)
                                    {
                                        AppDescription.Shutdown();
                                    }
                                    
                                    if(Game.Loaded)
                                    {
                                        Win32UnloadGame(&Game);
                                    }                                
                                    
                                    Game = Win32LoadGame(SourceGameCodeDLLFullPath, TempGameCodeDLLFullPath, GameCodeLockFullPath);
                                    
                                    if(AppDescription.Init)
                                    {
                                        AppDescription.Init(&RfPlatform);
                                    }
                                }
#endif
                                
                                // NOTE(zak): We reinit audio when the default audio device changes
                                if(ShouldReInitAudio)
                                {
                                    TerminateThread(AudioThreadHandle, 0);
                                    if(!Win32InitWASAPI())
                                    {
                                        RF_LOG("Unable to intialize WASAPI: %s", Win32GetLastErrorAsString());
                                    }
                                    
                                    ShouldReInitAudio = false;                                    
                                }
                                
                                // NOTE(zak): We only update NumControllerConnected on the first
                                // frame and when a controller is connected, this is because if you 
                                // call XInputGetState with  a index that isn't connected  a TON (millions)
                                // of cycles happen that do not happen if you call XInputGetState on a index
                                // with a controller connected                                
                                if(UpdateXInputControllerList)
                                {
                                    NumControllerConnected = 0;
                                    
                                    for(int Index = 0; Index < XUSER_MAX_COUNT; ++Index)
                                    {
                                        XINPUT_STATE State = {0};
                                        if(XInputGetState(Index, &State) != ERROR_DEVICE_NOT_CONNECTED)
                                        {
                                            ++NumControllerConnected;
                                        }
                                    }
                                    
                                    UpdateXInputControllerList = false;
                                }
                                
                                
                                SwitchToFiber(Win32MessageFiber);
                                
#if DEAR_IMGUI
                                if(ShouldUpdateAndDrawIMGUI)
                                {
                                    ImGui_ImplDX11_NewFrame();
                                    ImGui_ImplWin32_NewFrame();
                                    ImGui::NewFrame();
                                    
                                    //bool show_demo_window = 1;
                                    //ImGui::ShowDemoWindow(&show_demo_window);
                                }
#endif
                                
                                if(MouseMode == RF_MOUSE_MODE_GAME)
                                {                                    
                                    POINT Point = {0};
                                    Point.x = Win32GetWindowWidth() / 2;
                                    Point.y = Win32GetWindowHeight() / 2;
                                    ClientToScreen(Window, &Point);

                                    SetCursorPos(Point.x, Point.y);
                                }                                
                                
                                
                                for(int Index = 0; Index < NumControllerConnected; ++Index)
                                {
                                    XINPUT_STATE State = {0};
                                    XInputGetState(Index, &State);                                    
                                                                        
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_DPAD_UP);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_DPAD_DOWN);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_DPAD_LEFT);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_DPAD_RIGHT);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_START);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_BACK);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_LEFT_THUMB);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_RIGHT_THUMB);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_LEFT_SHOULDER);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_RIGHT_SHOULDER);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_A);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_B);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_X);
                                    Win32XInputProcessButton(&State, XINPUT_GAMEPAD_Y);                                    
                                                                        
                                    PreviousState = State;
                                }
                                
                                
                                // NOTE(zak): Is it okay if we only update and render the app if
                                // the window is selected
#if !DEBUG
                                if(GlobalIsWindowActive)
#endif
                                {   
                                    if(AppDescription.Update)
                                    {                                
                                        AppDescription.Update(DeltaTime, Events, CurrentEvent);
                                    }
                                    
#if DEAR_IMGUI
                                    if(ShouldUpdateAndDrawIMGUI)
                                    {
                                        ImGui::Render();
                                    }
#endif                      
                                    if(AppDescription.Render)
                                    {
                                        AppDescription.Render();                        
                                    }
                                    
#if DEAR_IMGUI
                                    if(ShouldUpdateAndDrawIMGUI)
                                    {
                                        ImGui_ImplDX11_RenderDrawData(ImGui::GetDrawData());
                                        
                                        if (IO.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
                                        {
                                            ImGui::UpdatePlatformWindows();
                                            ImGui::RenderPlatformWindowsDefault();
                                        }
                                    }
#endif
                                    
                                    // TODO(zak): !!!!Test this path!!!!!
                                    // NOTE(zak): This happens if the GPU was phyically removed or a driver updated
                                    // while the game was updating
                                    HRESULT PresentCode = D3D11Core.Swapchain->lpVtbl->Present(D3D11Core.Swapchain, AppDescription.ShouldVSync, 0);
                                    if((PresentCode == DXGI_ERROR_DEVICE_RESET) || (PresentCode ==DXGI_ERROR_DEVICE_REMOVED))
                                    {                                        
                                        AppDescription.Shutdown();
                                        Win32InitD3D11(Window);
                                        AppDescription.Init(&RfPlatform);
                                    }
                                }                                
                                
                                LARGE_INTEGER EndCounter = Win32GetWallClock();
                                rf_f32 MeasuredSecondsPerFrame = Win32GetSecondsElapsed(LastCounter, EndCounter);
                                rf_f32 ExactTargetFramesPerUpdate = MeasuredSecondsPerFrame*(rf_f32)MonitorRefreshHz;
                                rf_u32 NewExpectedFramesPerUpdate = RoundReal32ToInt32(ExactTargetFramesPerUpdate);
                                ExpectedFramesPerUpdate = NewExpectedFramesPerUpdate;
                                
                                TargetSecondsPerFrame = MeasuredSecondsPerFrame;
                                
                                LastCounter = EndCounter;
                                CurrentEvent = 0;
                            }
                        }
                        else
                        {
                            RF_LOG("ShowWindow failed with %s.", Win32GetLastErrorAsString());
                        }
                        
#if DEAR_IMGUI
                        ImGui_ImplDX11_Shutdown();
                        ImGui_ImplWin32_Shutdown();        
                        ImGui::DestroyContext();
#endif
                        
                        AppDescription.Shutdown();
                        
                                                
                        BOOL State  = 0;
                        D3D11Core.Swapchain->lpVtbl->GetFullscreenState(D3D11Core.Swapchain, &State, 0);

                        // NOTE(zak): If the Swapchain is fullscreen it
                        //needs to be changed to a windowed fullscreen
                        // before we can release it.
                        if(State)
                        {
                            D3D11Core.Swapchain->lpVtbl->SetFullscreenState(D3D11Core.Swapchain, 0, 0);
                        }
                        
                        SAFE_RELEASE(D3D11Core.RasterizerState);
                        SAFE_RELEASE(D3D11Core.DepthStencilState);
                        SAFE_RELEASE(D3D11Core.DepthStencilView);
                        SAFE_RELEASE(D3D11Core.RenderTargetView);
                        SAFE_RELEASE(D3D11Core.Swapchain);
                        SAFE_RELEASE(D3D11Core.DeviceContext);
                        SAFE_RELEASE(D3D11Core.Device);
                    }
                    else
                    {
                        RF_LOG("Win32InitD3D11 failed with %s.", Win32GetLastErrorAsString());
                    }                    
                }
                else
                {
                    RF_LOG("RegisterRawInputDevices failed with %s.", Win32GetLastErrorAsString());
                }                
            }
            else
            {
                RF_LOG("CreateWindowExA failed with %s.", Win32GetLastErrorAsString());
            }
        }
        else
        {
            RF_LOG("RegisterClassA failed to Register main window class with %s", Win32GetLastErrorAsString());
        }
    }        
    else
    {
        RF_LOG("Failed to Win32LoadGame %s.", Win32GetLastErrorAsString());
    } 
    
    return(0);
}

void
WinMainCRTStartup()
{
    int Result = WinMain(GetModuleHandle(0), 0, 0, 0);
    ExitProcess(Result);
}