#include "octree.h"

octree CreateOctree(u32 NodeSize)
{
    // Octree layout
    // 16 bits child pointer, 8 bit valid child, 8 bit leafs
    octree Octree = {.NodeSize = NodeSize}; 
    Octree.Data = (octree_info*)RfPlatform.AllocateMemory(sizeof(s32)*2000);
    u8 valid_children = 0b00110011;
    *(Octree.Data) = (octree_info){0, valid_children, valid_children};
    return Octree;
}

void MeshOctree(octree *Octree)
{
    cube * Cubes = (cube *)RfPlatform.AllocateMemory(sizeof(cube)*ChunkSizeSquared*ChunkSize);
    u32 CubeCount = 0;
    bool Valid = true;
    while(Valid)
    {
        AddCube(Cubes+CubeCount, X, Y, Z, 0, 0, Y*Y, 1, 1);
        CubeCount++;
    }
    u64 test2 = BlockCount;
    int VertexCount = CubeCount*36;
    D3D11_BUFFER_DESC BufferDescription = {0};
    BufferDescription.Usage = D3D11_USAGE_DEFAULT;
    BufferDescription.ByteWidth = sizeof(vertex) * VertexCount;
    BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
    BufferDescription.CPUAccessFlags = 0;
    
    // TODO(Horthrax): Interlocked
    VertexCountTotal+=VertexCount;
    
    D3D11_SUBRESOURCE_DATA SubresourceData = {0};
    SubresourceData.pSysMem = Cubes;
    if(FAILED(Device->lpVtbl->CreateBuffer(Device, &BufferDescription, &SubresourceData, &(World->ChunkInfo+Index)->VertexBuffer)))
    {
        // TODO(zak): Logging
    }
    (World->ChunkInfo+Index)->VertexCount = VertexCount;
    RfPlatform.FreeMemory(Cubes);
}