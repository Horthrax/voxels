#ifndef INPUT_HANDLER_H
#define INPUT_HANDLER_H

#include <stdio.h>
#include <stdlib.h>
#define STB_DS_IMPLEMENTATION
#include "stb_ds.h"

typedef struct input_types
{
    int MOVE_FORWARD = 1;
    int MOVE_BACKWARD = 2;
    int MOVE_LEFT = 3;
    int MOVE_RIGHT = 4;
    int MOVE_UP = 5;
    int MOVE_DOWN = 6;
    int DEBUG_MODE = 7;
} input_types;

struct {
    int key; 
    int value;
} *input_map = NULL;

static input_types Types;

void
InputInitialization()
{
    hmput(input_map, Types.MOVE_FORWARD, 'W');
    hmput(input_map, Types.MOVE_BACKWARD, 'S');
    hmput(input_map, Types.MOVE_LEFT, 'A');
    hmput(input_map, Types.MOVE_RIGHT, 'D');
    hmput(input_map, Types.MOVE_UP, 'Q');
    hmput(input_map, Types.MOVE_DOWN, 'E');
    hmput(input_map, Types.DEBUG_MODE, 0x72);
}

static bool Keycodes[255];

void
UpdateKeycode(int Keycode, bool IsDown)
{
    Keycodes[Keycode] = IsDown;
}

bool
IsInputTypeDown(int InputType)
{
    return Keycodes[hmget(input_map, InputType)];
}

#endif //INPUT_HANDLER_H
