// Copyright Rageface Studios, LTD. All Rights Reserved.

#ifndef VOXELS_H
#define VOXELS_H

typedef struct vertex
{
    u8 X;
    u8 Y;
    u8 Z;
    u8 XYZEmpty;
    u8 R;
    u8 G;
    u8 B;
    u8 RGBEmpty;
} vertex;

typedef struct constant_buffer
{
    hmm_mat4 WorldViewProjection;
    hmm_vec4 ChunkPosition;
} constant_buffer;

typedef struct camera
{
    hmm_vec3 Position;
    hmm_vec3 Front;
    hmm_vec3 Up;
    hmm_vec3 Target;
    
    float Yaw;
    float Pitch;
} camera;

typedef struct vertex_buffer_wrapper
{
    u64 VertexCount;
    ID3D11Buffer* VertexBuffer;
} vertex_buffer_wrapper;

typedef struct cube
{
    vertex Vertices[36];
} cube;

typedef struct voxel_face
{
    vertex Vertices[6];
} voxel_face;

typedef struct read_entire_file
{
    void *Contents;
    size_t ContentsSize;
} read_entire_file; 

#endif