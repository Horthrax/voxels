
#import <Cocoa/Cocoa.h>
#import <Metal/Metal.h>
#import <Metal/Metal.h>
#import <MetalKit/MetalKit.h>

@interface platform_layer_macos_app_delegate : NSObject<NSApplicationDelegate>
@end

@interface platform_layer_macos_window_delegate : NSObject<NSWindowDelegate>
@end

@interface platform_layer_macos_metal_view_delegate : NSObject<MTKViewDelegate>
@end

static platform_layer_macos_app_delegate *PlatformLayerMacOSAppdelegate;
static platform_layer_macos_window_delegate *PlatformLayerMacOSWindowDelegate;
static platform_layer_macos_metal_view_delegate *PlatformLayerMacOSMetalViewDelegate;

static void
PlatformLayer_Run()
{
    [NSApplication sharedApplication];
    NSApp.activationPolicy = NSApplicationActivationPolicyRegular;
    PlatformLayerMacOSAppdelegate = [[platform_layer_macos_app_delegate alloc] init];
    NSApp.delegate = PlatformLayerMacOSAppdelegate;
    [NSApp activateIgnoringOtherApps:YES];
    [NSApp run];
}

int
main(int ArgCount, char **ArgV)
{
    AppDescription = PlatformLayer_EntryPoint(ArgCount, ArgV);
    PlatformLayer_Run();
    return(0);
}

@implementation platform_layer_macos_app_delegate
- (void)applicationDidFinishLaunching:(NSNotification*)Notificaiton 
  {
      // Create Window
      NSUInteger WindowStyle = NSWindowStyleMaskTitled|NSWindowStyleMaskClosable|NSWindowStyleMaskMiniaturizable|NSWindowStyleMaskResizable;
      NSRect WindowRect = NSMakeRect(0, 0, AppDescription.WindowWidth, AppDescription.WindowHeight);
      NSWindow* Window = [[NSWindow alloc]
        initWithContentRect:WindowRect
        styleMask:WindowStyle
        backing:NSBackingStoreBuffered
        defer:NO];
      Window.title = [NSString stringWithUTF8String:AppDescription.WindowTitle];
      Window.acceptsMouseMovedEvents = YES;
      Window.restorable = YES;
      PlatformLayerMacOSWindowDelegate = [[platform_layer_macos_window_delegate alloc] init];
      Window.delegate = PlatformLayerMacOSWindowDelegate;

      // Initialize Metal
      id<MTLDevice> MetalDevice = MTLCreateSystemDefaultDevice();
      PlatformLayerMacOSMetalViewDelegate = [[platform_layer_macos_metal_view_delegate alloc] init];
      //MetalDevice.preferredFramesPerSecond = CGDisplayModeGetRefreshRate(CGDisplayCopyDisplayMode(Window.Screen))
      // Show Window

      [Window center];
      [Window makeKeyAndOrderFront:nil];
  }

@end

@implementation platform_layer_macos_window_delegate
- (BOOL)windowShouldClose:(id)Sender 
  {
      return YES;
  }
@end

@implementation platform_layer_macos_metal_view_delegate
- (void)drawInMTKView:(MTKView*)View
{

}

- (void)mtkView:(MTKView*)View drawableSizeWillChange:(CGSize)Size 
{
    /* this is required by the protocol, but we can't do anything useful here */
}

@end
