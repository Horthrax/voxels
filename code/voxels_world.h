#ifndef WORLD_H
#define WORLD_H

#define LOD 0 
#define CHUNK_HASH_SIZE 4096
#define CHUNK_SIZE_SQUARED 16384
#include <emmintrin.h>
#include <xmmintrin.h>
#include <smmintrin.h>

typedef struct voxel_rle
{
    u8 Length;
    u8 ID;
} voxel_rle;

typedef struct voxel
{
    u8 ID;
} voxel;

typedef struct position
{
    s32 X, Y, Z;  
} position;

// TODO(Horthrax): Maybe convert some of the u32s to size_t
typedef struct chunk
{
    u32 Index;
    position Position;
    u32 CurrentLength;
    u8 *RLEOffsetPositions;
} chunk;

typedef struct y_mesh_bounds
{
    u8 Minimum;
    u8 Maximum;
} y_mesh_bounds;

typedef enum voxel_direction {
    POSX,
    NEGX,
    POSY,
    NEGY,
    POSZ,
    NEGZ
} voxel_direction;

typedef struct chunk_info
{
    ID3D11Buffer *VertexBuffer;
    int VertexCount;
} chunk_info;

struct {
    hmm_vec3 key; // Vec3 Position
    chunk value; // World array offset
} *chunk_map = NULL;

typedef struct chunk_pos
{
    s32 X;
    s32 Y;
    s32 Z;
} chunk_pos;

typedef struct world
{
    u16 ChunkSize;
    u16 ChunkCount;
    u32 DefaultBufferSize;
    
    hmm_vec3 PlayerPosition;
    u8 *Chunks;
    chunk_info *ChunkInfo;
    chunk ChunkHash[CHUNK_HASH_SIZE];
} world;

void AddCube(cube *Cubes, u8 x, u8 y, u8 z, u8 r, u8 g, u8 b, u8 height, u8 width);
world InitializeWorld(u16 ChunkSize, u16 ChunkCount, u32 DefaultBufferSize);
inline u32 GetIndexFromPosition(hmm_vec3 Position, int Stride);
void MeshChunk(world *World, hmm_vec3 Position, u32 Index, ID3D11Device* Device);
chunk GetChunk(world * World, s32 ChunkX, s32 ChunkY, s32 ChunkZ);
int HashChunk(chunk_pos ChunkPosition);
void AddChunk(world * World, chunk Chunk);
void AddFace(voxel_face *Faces, voxel_direction VoxelDirection, u8 X, u8 Y, u8 Z, u8 R, u8 G, u8 B, u8 XAddition, u8 YAddition, u8 ZAddition);
static inline u32 rotl(u32 x, s8 r);
static inline u32 fmix(u32 h);
u32 m32 (const void *key, int len, u32 h1);
void UpdatePlayerPosition(world * World, hmm_vec3 Position);
inline u32 GetDecompressedIndex(u8 X, u8 Y, u8 Z, u8 Stride);
#endif //WORLD_H
