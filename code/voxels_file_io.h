// Copyright Rageface Studios, LTD. All Rights Reserved.

#ifndef VOXELS_FILE_IO_H
#define VOXELS_FILE_IO_H

typedef struct read_entire_file
{
    void *Contents;
    size_t ContentsSize;
} read_entire_file; 

static read_entire_file
ReadEntireFile(char *FilePath)
{
    read_entire_file Result = {0};
    
    rf_file File = RfPlatform.OpenFile(FilePath);
    if(File.IsValid)
    {
        size_t FileSize = RfPlatform.GetFileSize(&File);
        if(FileSize)
        {
            Result.Contents = RfPlatform.AllocateMemory(FileSize);
            if(Result.Contents)
            {                
                if(RfPlatform.ReadFile(&File, Result.Contents, FileSize))
                {
                    Result.ContentsSize = FileSize;
                }                
            }
        }
    }
    
    return(Result);
}

#endif // VOXELS_FILE_IO_