// Copyright Rageface Studios, LTD. All Rights Reserved.

#ifndef VOXEL_MATH_H
#define VOXEL_MATH_H

#define USE_SSE2
#include "sse_mathfun.h"

static float VoxelsSinF(float F)
{
    float TempF[4] = {F, 0, 0, 0};
    __m128 SIMDTHING = _mm_load_ps(TempF);
    v4sf Result = sin_ps(SIMDTHING);
    float result [4];
    _mm_store_ps(result, Result);        
    return(result[0]);
}


static float VoxelsCosF(float F)
{
    float TempF[4] = {F, 0, 0, 0};
    v4sf Result = cos_ps(_mm_load_ps(TempF));
    float result [4];
    _mm_store_ps(result, Result);        
    return(result[0]);
}


#endif // VOXEL_MATH_H