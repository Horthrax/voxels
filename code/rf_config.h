/* 
    ========================================================================
    File: rf_config.h 
    Date: Wed Nov 27 10:36:03 2019
    Creator: Zakary Strange 

    Notice: (C) Copyright 2019 by Parallel Games, LTD. All Rights Reserved 
    ========================================================================
*/ 

#ifndef RF_CONFIG_H
#define RF_CONFIG_H

#define DISABLE_HOT_RELOADING 1
#define DEBUG 1
#define WIREFRAME 1
#define SERVER 0

static rf_b32 ShouldUpdateAndDrawIMGUI;

#if DISABLE_HOT_RELOADING
#include "voxels.cc"
#endif

#endif // RF_CONFIG_H~