#include "rf_platform.h"
#include "rf_config.h"

#if _WIN32
#include "rf_platform_windows.cc"
#elif __APPLE__
#include "rf_platform_macos.cc"
#elif __linux__
#include "rf_platform_linux.cc"
#else
#error "Platform not supported"
#endif 

