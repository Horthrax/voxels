#include "voxels_world.h"

static int BlockCountTotal;
static int VertexCountTotal;

world
InitializeWorld(u16 ChunkSize, u16 ChunkCount, u32 DefaultBufferSize)
{
    world World = {.ChunkSize = ChunkSize, .ChunkCount = ChunkCount, .DefaultBufferSize = DefaultBufferSize};
    
    u64 WorseCaseChunkBlockAllocationAmount = DefaultBufferSize*ChunkCount*ChunkCount;
    u64 WorseCaseChunkInformationAllocationAmount = sizeof(chunk_info)*ChunkCount*ChunkCount;
    World.Chunks = (u8 *)RfPlatform.AllocateMemory(WorseCaseChunkBlockAllocationAmount);
    World.ChunkInfo = (chunk_info *)RfPlatform.AllocateMemory(WorseCaseChunkInformationAllocationAmount);
    return World;
}

static inline u32 
rotl32 (u32 x, s8 r) { return (x << r) | (x >> (32 - r)); }

static inline u32 
fmix (u32 h ) {
    h ^= h >> 16; h *= 0x85ebca6b;
    h ^= h >> 13; h *= 0xc2b2ae35;
    return h ^= h >> 16;
}

u32 
m32 (const void *key, int len, u32 h1) {
    const u8 *tail = (const u8*)((u8 *)key + (len/4)*4); // handle this separately
    
    u32 c1 = 0xcc9e2d51, c2 = 0x1b873593;
    
    // body (full 32-bit blocks) handled uniformly
    for (u32 *p = (u32*) key; p < (const u32*) tail; p++) {
        u32 k1 = *p; k1 *= c1; k1 = rotl32(k1,15); k1 *= c2; // MUR1
        h1 ^= k1; h1 = rotl32(h1,13); h1 = h1*5+0xe6546b64; // MUR2
    }
    
    u32 t = 0; // handle up to 3 tail bytes
    switch(len & 3) {
        case 3: t ^= tail[2] << 16;
        case 2: t ^= tail[1] << 8;
        case 1: {t ^= tail[0]; t *= c1; t = rotl32(t,15); t *= c2; h1 ^= t;};
    }
    return fmix(h1 ^ len);
}

int
HashChunk(chunk_pos ChunkPosition)
{
    int Hash = ChunkPosition.X << 13;
    Hash += ChunkPosition.Y >> 17;
    Hash += ChunkPosition.Z << 5;
    return Hash;
}

void
AddChunk(world * World, chunk Chunk)
{
    int Index = m32(&Chunk.Position, sizeof(chunk_pos), 0);
    int HashKey = Index & ArrayCount(World->ChunkHash);
    if(HashKey < CHUNK_HASH_SIZE)
    {
        World->ChunkHash[HashKey] = Chunk;
    }
    else
    {
        // !!!!!TODO(Horthrax): This was overwriting please fix this!!!!!
    }
}

chunk
GetChunk(world * World, s32 ChunkX, s32 ChunkY, s32 ChunkZ)
{
    // Using prime values isnt enough, spruce up later
    chunk_pos ChunkPosition = {ChunkX, ChunkY, ChunkZ};
    int Index = m32(&ChunkPosition.X, sizeof(chunk_pos), 0);
    int HashKey = Index & ArrayCount(World->ChunkHash);
    return(World->ChunkHash[HashKey]);
}

void
GenerateChunk(world * World, position Position, u32 Index)
{
    u16 ChunkSize = World->ChunkSize;
    size_t ChunkOffset = Index*World->DefaultBufferSize;
    // Since we dont know the amount of blocks before we generate any, 
    // the chunk information is postfixed onto the blocks in memory.
    u32 BlockCount = 0;
    //Since we want 2d terrain, we will sample 2d noise which is why we need only X and Z to be iterated through.
    u32 LastBlockCount = 0;
    chunk Chunk = {};
    Chunk.RLEOffsetPositions = (u8 *)RfPlatform.AllocateMemory(CHUNK_SIZE_SQUARED);
    for(int i = 0; i < ChunkSize*ChunkSize; i++)
    {
        // Some interesting noise types
        // stb_perlin_turbulence_noise3(Effect*XSample, 1.0, Effect*ZSample, Lacunarity, Gain, Octaves);
        // stb_perlin_fbm_noise3(Effect*XSample, 1.0, Effect*ZSample, Lacunarity, Gain, Octaves);
        // stb_perlin_ridge_noise3(Effect*XSample, 1.0, Effect*ZSample, Lacunarity, Gain, Offset, Octaves);
        // stb_perlin_noise3_internal((float)XSample,0.0,(float)ZSample,0,0,0,0);
        // stb_perlin_noise3(Effect*XSample, 1.0, Effect*ZSample, 0, 0, 0);
        
        int XSample = (i%ChunkSize);
        int ZSample = ((i/ChunkSize)%ChunkSize);
        float Lacunarity = 2.0;
        float Gain = 0.5;
        float Offset = 1.0; 
        int Octaves = 2;
        
        float Effect = 1.0/ChunkSize;
        
        //60.0 is a nice scale value
        //128 for 1024
        u8 Height = floorf(stb_perlin_turbulence_noise3(Effect*XSample, 1.0, Effect*ZSample, Lacunarity, Gain, Octaves)*24.0f);
        // Clamp the Height
        Height = Height > ChunkSize ? ChunkSize : Height;
        
        if(Height > 0)
        {
            *(voxel_rle *)(World->Chunks+ChunkOffset+(BlockCount*sizeof(voxel_rle))) = (voxel_rle){Height, 1};
            BlockCount++;
            if(Height < ChunkSize)
            {
                *(voxel_rle *)(World->Chunks+ChunkOffset+(BlockCount*sizeof(voxel_rle))) = (voxel_rle){ChunkSize-Height, 0};
                BlockCount++;
            }
        }
        else
        {
            *(voxel_rle *)(World->Chunks+ChunkOffset+(BlockCount*sizeof(voxel_rle))) = (voxel_rle){ChunkSize, 0};
            BlockCount++;
        }
        Chunk.RLEOffsetPositions[i] = (u8)BlockCount-LastBlockCount;
        LastBlockCount=BlockCount;
    }
    // Since we have to calculate the populated blocks before we can create the chunk struct, we add it post fixed to the blocks in memory.
    u32 BlockSizeInMemory = sizeof(voxel_rle)*BlockCount;
    assert(BlockSizeInMemory < World->DefaultBufferSize-sizeof(chunk));
    Chunk.Index = Index;
    Chunk.Position = Position;
    Chunk.CurrentLength = BlockSizeInMemory;
    //memcpy(Chunk.RLEOffsetPositions, RLEOffsetPositions, CHUNK_SIZE_SQUARED);
    u8 * EndStart = World->Chunks+ChunkOffset+World->DefaultBufferSize;
    *(chunk *)(World->Chunks+ChunkOffset+World->DefaultBufferSize-sizeof(chunk)) = Chunk;
    AddChunk(World, Chunk);
}

inline u32
GetIndexFromPosition(position Position, int Stride)
{
    return Position.X + Position.Z*Stride + Position.Y*Stride*Stride;
}

inline u32
GetDecompressedIndex(u8 X, u8 Y, u8 Z, u8 Stride)
{
    return X*Stride*Stride+Z*Stride+Y;
}

void
MeshChunk(world * World, position Position, u32 Index, ID3D11Device* Device){
    // Start at the lowest block in the chunk's buffer
    chunk Chunk = GetChunk(World, Position.X, Position.Y, Position.Z);
    u32 ChunkOffset = Index*World->DefaultBufferSize;
    voxel_rle *Blocks = (voxel_rle *)(World->Chunks+ChunkOffset);
    const u32 ChunkSize = World->ChunkSize;
    const u32 ChunkSizeSquared = ChunkSize*ChunkSize;
    u32 BlockCount = 32552;
    if(1){
        voxel_face * Faces = (voxel_face *)RfPlatform.AllocateMemory(sizeof(voxel_face)*ChunkSize*ChunkSize*ChunkSize);
        //memset(Faces, 0, sizeof(voxel_face)*ChunkSize*ChunkSize*ChunkSize);
        u32 CubeCount = 0;
        
        u32 FaceCount = 0;
        //https://youtu.be/4N3N1MlvVc4?t=6
        u128 LastOffset = 0;
        {
            u8 Heights[128] = {};
            for(int Depth = 0; Depth < ChunkSize; ++Depth)
            {
                int Voxels = 0;
                int Offset = 0;
                
                u128 VoxelBitmask[128] = {};
                u128 Mask[128] = {};
                y_mesh_bounds YMeshBounds = {ChunkSize, 0};
                while(Voxels < ChunkSizeSquared)
                {
                    voxel_rle Voxel = *(Blocks+LastOffset+Offset);
                    Offset++;
                    
                    if(Voxel.ID)
                    {
                        u8 Length = Voxel.Length;
                        if(Length > Heights[Voxels/128]){
                            VoxelBitmask[Voxels/128] |= (~(~0U << Length)) << Voxels%128;
                        }
                        if(Length > YMeshBounds.Maximum) YMeshBounds.Maximum = Length;
                        if(Length < YMeshBounds.Minimum) YMeshBounds.Minimum = Length;
                        Heights[Voxels/128] = Voxel.Length;
                    }
                    else if (Voxel.Length == 128)
                    {
                        Heights[Voxels/128] = 0;
                    }
                    Voxels+=Voxel.Length;
                }
                if(YMeshBounds.Minimum == 1) YMeshBounds.Minimum = 0;
                if(YMeshBounds.Maximum == 0) YMeshBounds.Maximum = ChunkSize;
                LastOffset += Offset;
                for(int Z = 0; Z < ChunkSize; ++Z)
                {
                    for(int Y = YMeshBounds.Minimum; Y < YMeshBounds.Maximum; ++Y)
                    {
                        u16 Search = Z*ChunkSize+Y;
                        u128 WorkingMask = Mask[Z];
                        u128 WorkingInt = VoxelBitmask[(Search)/128];
                        bool Valid =  WorkingInt >> Y;
                        u16 Height = 1;
                        u16 Width = 1;
                        if(Valid && !(WorkingMask >> Y & 1))
                        {
                            u128 VerticalOffset = 1;
                            bool VerticalNextValid = (WorkingInt >> (Y+VerticalOffset)) & 1;
                            while(VerticalNextValid && VerticalOffset+Y < ChunkSize && !(WorkingMask >> (Y+VerticalOffset) & 1))
                            {
                                Height++;
                                VerticalOffset++;
                                VerticalNextValid = (WorkingInt >> (Y+VerticalOffset)) & 1;
                            }
                            Mask[Z] |= (~(~0UL << VerticalOffset)) << Y;
                            
                            u128 HorizontalOffset = 1;
                            u128 CompareValue = (~(~0UL << VerticalOffset)) << Y;
                            u128 Access = (Search+(HorizontalOffset*ChunkSize))/128;
                            bool HorizontalValid = (CompareValue & VoxelBitmask[Access]) == CompareValue;
                            while(HorizontalValid && HorizontalOffset+Z < ChunkSize && (Mask[Z+HorizontalOffset] & CompareValue) == 0)
                            {
                                Mask[Z+HorizontalOffset] |= (~(~0UL << VerticalOffset)) << Y;
                                HorizontalOffset++;
                                Access = (Search+(HorizontalOffset*ChunkSize))/128;
                                HorizontalValid = (CompareValue & VoxelBitmask[Access]) == CompareValue;
                                Width++;
                            }
                            AddFace(Faces+FaceCount, NEGX, Depth, Y, Z, Width*100, (Depth%2)*255, 100, 1, Height, Width);
                            FaceCount++;
                        }
                    } 
                }
            }
        }
        u64 VertexCount = FaceCount*6;
        D3D11_BUFFER_DESC BufferDescription = {0};
        BufferDescription.Usage = D3D11_USAGE_DEFAULT;
        BufferDescription.ByteWidth = sizeof(vertex) * VertexCount;
        BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        BufferDescription.CPUAccessFlags = 0;
        D3D11_SUBRESOURCE_DATA SubresourceData = {0};
        SubresourceData.pSysMem = Faces;
        if(FAILED(Device->lpVtbl->CreateBuffer(Device, &BufferDescription, &SubresourceData, &(World->ChunkInfo+Index)->VertexBuffer)))
        {
            // TODO(zak): Logging
        }
        (World->ChunkInfo+Index)->VertexCount = VertexCount;
        RfPlatform.FreeMemory(Faces);
    }
    else
    {
        cube * Cubes = (cube *)RfPlatform.AllocateMemory(sizeof(cube)*ChunkSizeSquared*ChunkSize);
        u32 CubeCount = 0;
        
        int AllocationSize = ceil((ChunkSize*ChunkSize*ChunkSize)/128.0f);
        u128 VoxelBitmask[AllocationSize];
        memset(VoxelBitmask, 0, AllocationSize*sizeof(u128));
        
        u64 Offset = 0;
        u64 FinalBlockCount = 0;
        for(int i = 0; i < BlockCount; ++i)
        {
            voxel_rle Voxel = *(Blocks+i);
            if(Voxel.ID)
            {
                VoxelBitmask[Offset/128] |= (~(~0U << Voxel.Length)) << Offset%128;
                FinalBlockCount++;
            }
            Offset+=Voxel.Length;
        }
        u64 test = FinalBlockCount;
        for(int i = 0; i < ChunkSize*ChunkSize*ChunkSize; ++i)
        {
            bool NotAir = VoxelBitmask[i/128] >> i%128 & 1;
            if(NotAir)
            {
                int Y = (i%ChunkSize);
                int Z = ((i/ChunkSize)%ChunkSize);
                int X = (((i/ChunkSize)/ChunkSize)%ChunkSize);
                AddCube(Cubes+CubeCount, X, Y, Z, 0, 0, Y*Y, 1, 1);
                CubeCount++;
            }
        }
        u64 test2 = BlockCount;
        int VertexCount = CubeCount*36;
        D3D11_BUFFER_DESC BufferDescription = {0};
        BufferDescription.Usage = D3D11_USAGE_DEFAULT;
        BufferDescription.ByteWidth = sizeof(vertex) * VertexCount;
        BufferDescription.BindFlags = D3D11_BIND_VERTEX_BUFFER;
        BufferDescription.CPUAccessFlags = 0;
        
        // TODO(Horthrax): Interlocked
        VertexCountTotal+=VertexCount;
        
        D3D11_SUBRESOURCE_DATA SubresourceData = {0};
        SubresourceData.pSysMem = Cubes;
        if(FAILED(Device->lpVtbl->CreateBuffer(Device, &BufferDescription, &SubresourceData, &(World->ChunkInfo+Index)->VertexBuffer)))
        {
            // TODO(zak): Logging
        }
        (World->ChunkInfo+Index)->VertexCount = VertexCount;
        RfPlatform.FreeMemory(Cubes);
    }
}


void
AddFace(voxel_face *Faces, voxel_direction VoxelDirection, u8 x, u8 y, u8 z, u8 r, u8 g, u8 b, u8 XAddition, u8 YAddition, u8 ZAddition)
{
    voxel_face * Face = Faces;
    
    // Sort order
    // NegZ
    // PosZ
    // NegY
    // PosY
    // PosX
    // NegX
    
    switch(VoxelDirection)
    {
        case POSX:
        {
            Face->Vertices[0] = (vertex){x+XAddition, y, z, 0, r, g, b, 0};
            Face->Vertices[1] = (vertex){x+XAddition, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[2] = (vertex){x+XAddition, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[3] = (vertex){x+XAddition, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[4] = (vertex){x+XAddition, y, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[5] = (vertex){x+XAddition, y, z, 0, r, g, b, 0};
        }
        break;
        case NEGX:
        {
            Face->Vertices[0] = (vertex){x, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[1] = (vertex){x, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[2] = (vertex){x, y, z, 0, r, g, b, 0};
            Face->Vertices[3] = (vertex){x,y,z, 0, r, g, b, 0};
            Face->Vertices[4] = (vertex){x,y,z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[5] = (vertex){x, y+YAddition, z+ZAddition, 0, r, g, b, 0};
        }
        break;
        case POSZ:
        {
            Face->Vertices[0] = (vertex){x, y, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[1] = (vertex){x+XAddition, y, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[2] = (vertex){x+XAddition, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[3] = (vertex){x+XAddition, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[4] = (vertex){x, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[5] = (vertex){x, y, z+ZAddition, 0, r, g, b, 0};
        }
        break;
        case NEGZ:
        {
            Face->Vertices[0] = (vertex){x+XAddition, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[1] = (vertex){x+XAddition, y, z, 0, r, g, b, 0};
            Face->Vertices[2] = (vertex){x, y, z, 0, r, g, b, 0};
            Face->Vertices[5] = (vertex){x+XAddition, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[4] = (vertex){x, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[3] = (vertex){x, y, z, 0, r, g, b, 0};
        }
        break;
        case POSY:
        {
            Face->Vertices[0] = (vertex){x+XAddition, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[1] = (vertex){x+XAddition, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[2] = (vertex){x, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[3] = (vertex){x, y+YAddition, z, 0, r, g, b, 0};
            Face->Vertices[4] = (vertex){x, y+YAddition, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[5] = (vertex){x+XAddition, y+YAddition, z+ZAddition, 0, r, g, b, 0};
        }
        break;
        case NEGY:
        {
            Face->Vertices[0] = (vertex){x, y, z, 0, r, g, b, 0};
            Face->Vertices[1] = (vertex){x+XAddition, y, z, 0, r, g, b, 0};
            Face->Vertices[2] = (vertex){x+XAddition, y, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[3] = (vertex){x+XAddition, y, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[4] = (vertex){x, y, z+ZAddition, 0, r, g, b, 0};
            Face->Vertices[5] = (vertex){x, y, z, 0, r, g, b, 0};
        }
        break;
    }
}

void 
AddCube(cube * Cubes, u8 x, u8 y, u8 z, u8 r, u8 g, u8 b, u8 height, u8 width)
{
    cube * Cube = Cubes;
    Cube->Vertices[0] = (vertex){x+width, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[1] = (vertex){x+width, y, z, 0, r, g, b, 0};
    Cube->Vertices[2] = (vertex){x, y, z, 0, r, g, b, 0};
    Cube->Vertices[5] = (vertex){x+width, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[4] = (vertex){x, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[3] = (vertex){x, y, z, 0, r, g, b, 0};
    
    Cube->Vertices[6] = (vertex){x, y, z+width, 0, r, g, b, 0};
    Cube->Vertices[7] = (vertex){x+width, y, z+width, 0, r, g, b, 0};
    Cube->Vertices[8] = (vertex){x+width, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[9] = (vertex){x+width, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[10] = (vertex){x, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[11] = (vertex){x, y, z+width, 0, r, g, b, 0};
    
    Cube->Vertices[12] = (vertex){x, y, z, 0, r, g, b, 0};
    Cube->Vertices[13] = (vertex){x+width, y, z, 0, r, g, b, 0};
    Cube->Vertices[14] = (vertex){x+width, y, z+width, 0, r, g, b, 0};
    Cube->Vertices[15] = (vertex){x+width, y, z+width, 0, r, g, b, 0};
    Cube->Vertices[16] = (vertex){x, y, z+width, 0, r, g, b, 0};
    Cube->Vertices[17] = (vertex){x, y, z, 0, r, g, b, 0};
    
    Cube->Vertices[18] = (vertex){x+width, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[19] = (vertex){x+width, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[20] = (vertex){x, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[21] = (vertex){x, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[22] = (vertex){x, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[23] = (vertex){x+width, y+height, z+width, 0, r, g, b, 0};
    
    Cube->Vertices[24] = (vertex){x+width, y, z, 0, r, g, b, 0};
    Cube->Vertices[25] = (vertex){x+width, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[26] = (vertex){x+width, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[27] = (vertex){x+width, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[28] = (vertex){x+width, y, z+width, 0, r, g, b, 0};
    Cube->Vertices[29] = (vertex){x+width, y, z, 0, r, g, b, 0};
    
    Cube->Vertices[30] = (vertex){x, y+height, z+width, 0, r, g, b, 0};
    Cube->Vertices[31] = (vertex){x, y+height, z, 0, r, g, b, 0};
    Cube->Vertices[32] = (vertex){x, y, z, 0, r, g, b, 0};
    Cube->Vertices[33] = (vertex){x,y,z, 0, r, g, b, 0};
    Cube->Vertices[34] = (vertex){x,y,z+width, 0, r, g, b, 0};
    Cube->Vertices[35] = (vertex){x, y+height, z+width, 0, r, g, b, 0};
}

void
UpdatePlayerPosition(world * World, hmm_vec3 PlayerPosition)
{
    World->PlayerPosition = PlayerPosition;    
}
