cbuffer CONSTANT_BUFFER : register(b0)
{
    float4x4 WorldViewProjection;
    float4 ChunkPosition;
};

struct VertexInput
{
     uint4 position: POSITION;
     uint4 color : COLOR;
};

struct PixelInput
{
    float4 position : SV_POSITION;
    float4 color : COLOR; 
};

 PixelInput VS(VertexInput input)
{
    PixelInput output;
    float x = float(input.position.x);
    float y = float(input.position.y);
    float z = float(input.position.z);
    // NOTE(Horthrax): Be sure to update this multiplier to the chunk dimension, maybe a buffer object?
    float chunk_size = ChunkPosition.w;
    float4 position = mul(WorldViewProjection, float4(x+ChunkPosition.x*chunk_size, y+ChunkPosition.y*chunk_size, z+ChunkPosition.z*chunk_size, 1.0f));
    //float4 position = mul(WorldViewProjection, float4(x, y, z, 0.0f));
    //float4 position = float4(x, y, z, 0.0f);
    output.position = position;
    float RGBEffect = 1.0f/255.0f;
    float r = float(input.color.r);
    float g = float(input.color.g);
    float b = float(input.color.b);
    // TODO(Horthrax): add transparency
    output.color = float4(RGBEffect*r, RGBEffect*g, RGBEffect*b, 1.0f);
    return output;
}