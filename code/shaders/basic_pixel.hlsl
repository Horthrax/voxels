struct PixelInput
{
    float4 position : SV_POSITION;
    float4 color : COLOR; 
};

float4 PS(PixelInput input) : SV_TARGET
{
    return pow(abs(input.color), 2.2);
}