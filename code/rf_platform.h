#ifndef RF_PLATFORM_LAYER_H
#define RF_PLATFORM_LAYER_H

#define RF_PLATFORM_EXISTS

#include <stdint.h>
#include <stddef.h> // For size_t on some platforms (clang on mac)
#include <stdarg.h>
#include <stdbool.h>

#define RR_SPRINTF_IMPLEMENTATION
#include "rrsprintf.h"

// TODO(zak): Do all compilers support dllexport yet?
#if !DISABLE_HOT_RELOADING
#if _WIN32
#if __cplusplus
#define PL_DLL_EXPORT extern "C" __declspec(dllexport)
#else 
#define PL_DLL_EXPORT __declspec(dllexport)
#endif // __cplusplus
#else
#define PL_DLL_EXPORT
#endif // _WIN32
#endif // DISABLE_HOT_RELOADING

#define RfPlatform_Assert(Expression) if(!(Expression)) { *(volatile int *)0 = 0; }
#define FUCKYOUTHREADSIZE 16384

typedef uint8_t rf_u8;
typedef uint16_t rf_u16;
typedef uint32_t rf_u32;
typedef uint32_t rf_u64;

typedef int8_t rf_s8;
typedef int16_t rf_s16;
typedef int32_t rf_s32;
typedef int64_t rf_s64;

typedef int32_t rf_b32;

typedef float rf_f32;
typedef float rf_f64;

#if !__cplusplus
#define false 0
#define true 1
#endif

#define ArrayCount(Array) (sizeof(Array) / sizeof((Array)[0]))

typedef struct rf_platform_layer_app_description rf_platform_layer_entrypoint(int ArgCount, char **ArgV);
typedef void rf_platform_layer_init(struct rf_platform *InPlatform);
typedef void rf_platform_layer_shutdown();
typedef void rf_platform_layer_update(float dt, struct rf_platform_event *Events, unsigned int EventCount);
typedef void rf_platform_layer_render();
typedef void rf_platform_audio_callback(struct rf_platform_audio *Audio);

typedef struct rf_platform_layer_app_description
{
    // Called after Window is shown and Graphics context is created
    rf_platform_layer_init *Init;
    rf_platform_layer_shutdown *Shutdown;
    rf_platform_layer_update *Update;
    rf_platform_layer_render *Render;
    
    // NOTE(zak): Called every nanosecond? on its own thread
    rf_platform_audio_callback *AudioCallback;
    
    rf_s32 WindowWidth;
    rf_s32 WindowHeight;
    rf_b32 IsFullscreen;
    rf_b32 NeedsDepthBuffer;
    rf_b32 NeedsStencilBuffer;
    rf_b32 ShouldVSync;
    rf_b32 NeedsAudio;
    rf_b32 NeedsNetworking;
    
    char *WindowTitle;
} rf_platform_layer_app_description;

typedef enum rf_platform_event_type
{
    RF_EVENT_TYPE_KEYBOARD,
    RF_EVENT_TYPE_MOUSE,
    RF_EVENT_TYPE_GAMEPAD,
    RF_EVENT_TYPE_RESIZE   
} rf_platform_event_type;

typedef struct rf_platform_keyboard_event_info
{
    rf_u32 KeyCode;
    rf_b32 IsDown;
} rf_platform_keyboard_event_info;

typedef enum rf_platform_mouse_event_type
{
    MOUSE_EVENT_MOVE,
    MOUSE_EVENT_INPUT,    
} rf_platform_mouse_event_type;

typedef enum rf_platform_mouse_button
{
    RF_PLATFORM_MOUSE_BUTTON_LEFT,
    RF_PLATFORM_MOUSE_BUTTON_MIDDLE,
    RF_PLATFORM_MOUSE_BUTTON_RIGHT,
 
} rf_platform_mouse_button;

typedef struct rf_platform_mouse_event_info
{
    rf_platform_mouse_event_type Type;
    rf_u32 MouseX, MouseY;
    rf_platform_mouse_button MouseButton;
    rf_b32 IsDown;
} rf_platform_mouse_event_info;

typedef enum rf_platform_gamepad_button 
{
    RF_PLATFORM_GAMEPAD_DPAD_UP,
    RF_PLATFORM_GAMEPAD_DPAD_DOWN,
    RF_PLATFORM_GAMEPAD_DPAD_LEFT,
    RF_PLATFORM_GAMEPAD_DPAD_RIGHT,
    RF_PLATFORM_GAMEPAD_START,
    RF_PLATFORM_GAMEPAD_BACK,
    RF_PLATFORM_GAMEPAD_LEFT_THUMB,
    RF_PLATFORM_GAMEPAD_RIGHT_THUMB,
    RF_PLATFORM_GAMEPAD_LEFT_SHOULDER,
    RF_PLATFORM_GAMEPAD_RIGHT_SHOULDER,
    RF_PLATFORM_GAMEPAD_A,
    RF_PLATFORM_GAMEPAD_B,
    RF_PLATFORM_GAMEPAD_X,
    RF_PLATFORM_GAMEPAD_Y,    
} rf_platform_gamepad_button;

typedef enum rf_platform_gamepad_event_type
{
    RF_PLATFORM_GAMEPAD_EVENT_TYPE_JOYSTICK,
    RF_PLATFORM_GAMEPAD_EVENT_TYPE_BUTTON,
} rf_platform_gamepad_event_type;

typedef struct rf_platform_gamepad_event_info
{
    rf_platform_gamepad_event_type Type;
    
    float LeftJoystickX, LeftJoystickY;
    float RigthtJoystickX, RightJoystickY;
    
    rf_platform_gamepad_button Button;
    rf_b32 IsDown;
} rf_platform_gamepad_event_info;

typedef struct rf_platfrom_resize_event_info
{
    rf_u32 Width;
    rf_u32 Height;
} rf_platfrom_resize_event_info;

typedef struct rf_platform_event
{
    rf_platform_event_type Type;    
    rf_platform_keyboard_event_info KeyboardInfo;
    rf_platform_mouse_event_info MouseInfo;
    rf_platform_gamepad_event_info GamepadInfo;
    rf_platfrom_resize_event_info ResizeInfo;
} rf_platform_event;

typedef struct rf_platform_audio
{
    rf_u32 ChannelCount;
    rf_u32 SampleRate;
    rf_s32 SampleCount;
    
    rf_s16 *Buffer;
} rf_platform_audio;

typedef struct rf_platform_file
{    
    bool IsValid;
    struct rf_platform_file_handle *Handle;
} rf_file;

typedef void *rf_d3d_get_device();
typedef void *rf_d3d_get_device_context();
typedef void *rf_d3d_get_render_target_view();
typedef void *rf_d3d_get_depth_stencil_view();
typedef rf_u32 rf_d3d_get_feature_level();

// TODO(zak): Add support here for file creation at some point by passing in flags to open file
typedef rf_file rf_open(char *FileName);
typedef size_t rf_get_file_size(rf_file *File);
typedef rf_b32 rf_read_file(rf_file *File, void *Buffer, size_t BytesToRead);
typedef rf_b32 rf_write_file(rf_file *File, void *Buffer, size_t BufferLength);
typedef void rf_log(char *String);

typedef void *rf_allocate_memory(size_t Size);
typedef void rf_free_memory(void *Memory);

struct rf_platform_work_queue;
#define RF_PLATFORM_WORK_QUEUE_CALLBACK(name) void name(rf_platform_work_queue *Queue, void *Data)
typedef RF_PLATFORM_WORK_QUEUE_CALLBACK(rf_platform_work_queue_callback); 
typedef void rf_add_job_to_queue(rf_platform_work_queue *WorkQueue, rf_platform_work_queue_callback *Callback, void *Data);
typedef void rf_add_job_to_high_priority_queue(rf_platform_work_queue_callback *Callback, void *Data);
typedef void rf_add_job_to_low_priority_queue(rf_platform_work_queue_callback *Callback, void *Data);
typedef void rf_complete_all_jobs_in_queue(rf_platform_work_queue *WorkQueue);

typedef rf_u32 rf_get_window_width(void);
typedef rf_u32 rf_get_window_height(void);

typedef enum rf_platform_mouse_mode
{
    RF_MOUSE_MODE_APPLICATION,
    RF_MOUSE_MODE_GAME,        
} rf_mouse_mode;

typedef void rf_set_mouse_mode(rf_mouse_mode Mode);

typedef struct rf_platform_image
{
    int Width;
    int Height;
    void *Data;
} rf_platform_image;

typedef rf_platform_image rf_load_image(char *FileName);
typedef void rf_platform_set_draw_imgui(bool ShouldDrawImgui);

typedef struct rf_platform
{   
    rf_d3d_get_device *D3DGetDevice;    
    rf_d3d_get_device_context *D3DGetDeviceContext;
    rf_d3d_get_render_target_view *D3DGetRenderTargetView;
    rf_d3d_get_depth_stencil_view *D3DGetDepthStencilView;
    rf_d3d_get_feature_level *D3DGetFeatureLevel;
    
    rf_open *OpenFile;
    rf_get_file_size *GetFileSize;
    rf_read_file *ReadFile;
    rf_write_file *WriteFile;
    rf_log *Log;
    
    rf_allocate_memory *AllocateMemory;
    rf_free_memory *FreeMemory;
    
    rf_add_job_to_high_priority_queue *AddJobToHighPriorityQueue;
    rf_add_job_to_low_priority_queue *AddJobToLowPriorityQueue;
    rf_add_job_to_queue *AddJobToQueue;
    rf_complete_all_jobs_in_queue *CompleteAllJobsInQueue;
    
    
    // TODO(zak): add support for creating these in the application layer
    rf_platform_work_queue *HighPriorityQueue;
    rf_platform_work_queue *LowPriorityQueue;    
        
    rf_get_window_width *GetWindowWidth;
    rf_get_window_height *GetWindowHeight;
    rf_set_mouse_mode *SetMouseMode;
    
    rf_load_image *LoadImage;
    rf_platform_set_draw_imgui *SetShouldDrawIMGUI;
} rf_platform;

// NOTE(zak): This is for the application to callback into the platform layer
static rf_platform RfPlatform;


static void 
RfLog(char *Filename, int LineNumber, const char *Format, ...)
{
    // TODO(zak): These are retoally large lets move these down at somepoint
    char InputFormated[1024*2];
    
    va_list Arguments;
    va_start(Arguments, Format);
    rrvsprintf(InputFormated, Format, Arguments);
    va_end(Arguments);
    
    // TODO(zak): These are really large lets move these down at somepoint
    char Log[1024*4];
    rrsprintf(Log, "%s(%i): %s\n", Filename, LineNumber, InputFormated);
    RfPlatform.Log(Log);    
}

#define RF_LOG(String, ...) RfLog(__FILE__, __LINE__, String, ##__VA_ARGS__)

#endif // RF_PLATFORM_LAYER_H
