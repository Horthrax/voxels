#ifndef MEMORY_ALLOCATORS_H
#define MEMORY_ALLOCATORS_H

#include "memory_types.h"

typedef struct pool_arena
{
    u8* Buffer;
    size_t Stride;
    size_t Offset;
    size_t BufferLength;
} pool_arena;

void PoolInit(pool_arena *Arena, void *BackingBuffer, size_t Stride, size_t BufferLength);
inline size_t GetCurrentPool(pool_arena Arena);
void SetPoolOffset(pool_arena *Arena, size_t Pool);
void *GetPool(pool_arena *Arena, size_t Pool);

#endif //MEMORY_ALLOCATORS_H
