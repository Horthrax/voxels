@echo off

REM Copyright Rageface Studios, LTD. All Rights Reserved.
REM TODO(zak): Eventually add a mode where we build with without Dear, Imgui
REM as it will not be used for shipping. Also eventually switch us over to CRT less

FOR /F "usebackq tokens=2,* skip=2" %%L IN (
    `reg query "HKEY_LOCAL_MACHINE\SOFTWARE\Microsoft\Windows Kits\Installed Roots" /v KitsRoot10`
) DO SET sdkpath=%%M


FOR /F "usebackq tokens=2,* skip=2" %%L IN (
    `reg query "HKEY_LOCAL_MACHINE\SOFTWARE\WOW6432Node\Microsoft\VisualStudio\SxS\VS7" /v 15.0`
) DO SET vs_sdkpath=%%M


SET p="%sdkpath%Lib\"
for /D %%x in (%p%*) do if not defined f set "f=%%x"
SET um_compiler_path=%f%\ucrt\x64
SET ucrt_compiler_path=%f%\um\x64

SET p="%vs_sdkpath%VC\Tools\MSVC\"
for /D %%x in (%p%*) do if not defined z set "z=%%x"
set vc_lib=%z%\lib\x64

IF NOT EXIST ..\build mkdir ..\build


pushd shaders
fxc /nologo /EVS /Tvs_4_0 /Fo basic_vertex.hlslc basic_vertex.hlsl
fxc /nologo /EPS /Tps_4_0 /Fo basic_pixel.hlslc basic_pixel.hlsl
popd shaders


pushd ..\build

set CommonCompilerFlags=-g -c -fno-stack-protector -fdiagnostics-absolute-paths -O0 -mincremental-linker-compatible -Wno-incompatible-pointer-types -Wno-visibility -Wno-deprecated -Wno-unused-value -Wno-writable-strings -Wno-c++11-narrowing -flto=thin -DDEAR_IMGUI=1 -msse4.1 -Wno-error=shift-count-overflow

IF NOT EXIST imgui.o clang %CommonCompilerFlags% ../code/imgui.cpp
IF NOT EXIST imgui_demo.o clang %CommonCompilerFlags% ../code/imgui_demo.cpp
IF NOT EXIST imgui_draw.o clang %CommonCompilerFlags% ../code/imgui_draw.cpp
IF NOT EXIST imgui_impl_dx11.o clang %CommonCompilerFlags% ../code/imgui_impl_dx11.cpp
IF NOT EXIST imgui_impl_win32.o clang %CommonCompilerFlags% ../code/imgui_impl_win32.cpp
IF NOT EXIST imgui_widgets.o clang %CommonCompilerFlags% ../code/imgui_widgets.cpp

clang %CommonCompilerFlags% ../code/rf_platform.cc 

lld-link /debug /libpath:"%um_compiler_path%" /libpath:"%ucrt_compiler_path%" /libpath:"%vc_lib%" /out:win32_voxels.exe rf_platform.o imgui.o imgui_demo.o imgui_draw.o imgui_impl_dx11.o imgui_impl_win32.o imgui_widgets.o d3d11.lib dxgi.lib dxguid.lib user32.lib shell32.lib ole32.lib hid.lib gdi32.lib Ws2_32.lib Windowscodecs.lib msvcrt.lib

REM 

popd
